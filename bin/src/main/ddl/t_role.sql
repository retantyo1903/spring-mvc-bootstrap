--------------------------------------------------------
--  File created - Jumat-Desember-28-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table T_ROLE
--------------------------------------------------------

  CREATE TABLE "T_ROLE" 
   (	"ID" NUMBER(11,0), 
	"CODE" VARCHAR2(50 BYTE), 
	"NAME" VARCHAR2(50 BYTE), 
	"DESCRIPTION" VARCHAR2(255 BYTE), 
	"CREATED_BY" NUMBER(11,0), 
	"CREATED_ON" DATE, 
	"MODIFIED_BY" NUMBER(11,0), 
	"MODIFIED_ON" DATE, 
	"DELETED_BY" NUMBER(11,0), 
	"DELETED_ON" DATE, 
	"IS_DELETE" NUMBER(1,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index T_ROLE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "T_ROLE_PK" ON "T_ROLE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Trigger T_ROLE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "T_ROLE_TRG" 
BEFORE INSERT ON T_ROLE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT T_ROLE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "T_ROLE_TRG" ENABLE;
--------------------------------------------------------
--  Constraints for Table T_ROLE
--------------------------------------------------------

  ALTER TABLE "T_ROLE" MODIFY ("CREATED_ON" NOT NULL ENABLE);
  ALTER TABLE "T_ROLE" MODIFY ("CREATED_BY" NOT NULL ENABLE);
  ALTER TABLE "T_ROLE" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "T_ROLE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_ROLE" MODIFY ("CODE" NOT NULL ENABLE);
  ALTER TABLE "T_ROLE" ADD CONSTRAINT "T_ROLE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "T_ROLE" MODIFY ("IS_DELETE" NOT NULL ENABLE);
