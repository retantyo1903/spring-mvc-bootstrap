<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Test List</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${contextName}/assets/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script
	src="${contextName}/assets/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${contextName}/assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
	var result = '';
	
	function array(i, e) {
		result += '<tr>';
		result += '<td>'+e.name+'</td>';
		result += '<td>'+e.user.username+'</td>';
		result += '<td>list</td>'
		result += '</tr>';
	}
	
	function show(d) {
		result = '';
		$(d).each(array);
		$('#testList').html(result);
	}
	
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/test/',
			success : function(d) {
				show(d);
			}
		});
	}
</script>

<script>
	$(function() {
		$('#example1').DataTable()
		$('#example2').DataTable({
			'paging' : true,
			'lengthChange' : false,
			'searching' : false,
			'ordering' : true,
			'info' : true,
			'autoWidth' : false
		})
	})
</script>
</head>
<body>
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>TEST</h1>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">TEST LIST</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="col-xs-11">
								<form role="form">
									<div class="col-xs-3">
										<div class="form-group">
											<input type="text" class="form-control" id="searchBar"
												placeholder="Search by name">
										</div>
									</div>
									<button type="button" class="btn btn-primary">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</form>
							</div>
							<div class="col-xs-1">
								<button type="button" class="btn btn-warning">
									<i class="glyphicon glyphicon-plus"></i>
								</button>
							</div>
							<div></div>
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>NAME</th>
										<th>CREATED BY</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="testList">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	
	<script>
		$(document).ready(function() {
			loadData();
		});
	</script>
</body>
</html>

