<form role="form" id="technologyDelete">
	<div class="box-body">

		<input type="text" class="form-control" name="id" id="delete-id"
			value="${technology.id}"> <input type="hidden"
			class="form-control" name="createdBy" id="delete-createdBy"
			value="${technology.createdBy}"> <input type="hidden"
			class="form-control" name="createdOn" id="delete-createdOn"
			value="${technology.createdOn}"> <input type="hidden"
			class="form-control" name="name" id="delete-name"
			value="${technology.name}">
			<input type="hidden" class="form-control" name="notes" id="delete-notes" value="${technology.notes}">
			


		<h3>Are You Sure Want To Delete Technology ${technology.name } ?</h3>

	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
