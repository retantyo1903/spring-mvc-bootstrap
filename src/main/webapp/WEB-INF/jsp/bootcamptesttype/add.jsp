<form id="formAdd">

	<div class="form-group">
		<input type="text" class="form-control" name="name" id="add-name"
			placeholder="Name">
	</div>

	<div class="form-group">
		<textarea name="notes" id="add-notes" class="form-control"
			placeholder="Description"></textarea>
	</div>

	<button class="btn btn-primary" type="button" onclick="simpanData()">Proses</button>

	<button class="btn btn-danger" type="reset">Reset</button>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>