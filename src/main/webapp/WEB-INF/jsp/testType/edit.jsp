
<form id="formEdit">
	<div class="box-body">


		<input type="hidden" class="form-control" name="id" id="edit-id"
			value="${testType.id}"> <input type="hidden"
			class="form-control" name="createdBy" id="edit-createdBy"
			value="${testType.createdBy}"> <input type="hidden"
			class="form-control" name="modifiedBy" id="edit-modifiedBy"
			value="${testType.modifiedBy}"> <input type="hidden"
			class="form-control" name="createdOn" id="edit-createdOn"
			value="${testType.createdOn}"> <input type="hidden"
			class="form-control" name="modifiedOn" id="edit-modifiedOn"
			value="${testType.modifiedOn}"> <input type="hidden"
			class="form-control" name="isDelete" id="edit-isDelete"
			value="${testType.isDelete}"> <input type="hidden"
			class="form-control" name="typeOfAnswer" id="edit-typeOfAnswer"
			value="${testType.typeOfAnswer}">


		<div class="form-group">
			<label>Name</label> <input class="form-control" name="name"
				id="edit-name" value="${testType.name}">

		</div>
		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="edit-note" id="edit-note">${testType.note}</textarea>
		</div>
	</div>
	<!-- /.box-body -->
	<div class="box-footer pull-right">
		<button type="submit" class="btn btn-warning"
			>Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>