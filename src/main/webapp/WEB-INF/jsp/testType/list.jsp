
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">TEST TYPE</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="col-xs-11">
			<form role="form">
				<div class="col-xs-4">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" id="searchBar"
								placeholder="Search By Name"> <span
								class="input-group-btn">
								<button type="button" class="btn btn-warning"
									onclick="searchfunct()">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-xs-1">
			<button type="button" id="button-add" class="btn btn-sm btn-warning">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>
		<div></div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CREATED BY</th>
					<th>OPSI</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="testTypeList" align="center">
			</tbody>
		</table>
	</div>

</div>
<!-- Modal -->
<!-- Modal-begin -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<!-- Modal content-->
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>
<!-- Modal-end -->


<%@ include file="search.jsp"%>
<script>
	$("#button-add").click(function() {
		$.ajax({
			url : '${contextName}/test-type/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$('#myModal').find('.box-title').html('Add Test Type');
			}
		});
	});

	function array(i, e) {
		result += '<tr>';
		result += '<td><p>' + e.name + '</p></td>';
		result += '<td>' + e.user.username + '</td>';
		result += '<td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-menu-hamburger"></i></button><ul class="dropdown-menu"><li><a href="#" onclick="getOneData('
				+ e.id
				+ ')"><i class="fa fa-edit"></i>Edit</a></li><li><a href="#" onclick="getDelData('
				+ e.id
				+ ')"><i class="fa fa-exclamation-circle"></i>Delete</a></li></ul></div></td>'
		result += '</tr>';
	}
	function getOneData(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/test-type/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$('#myModal').find('.box-title').html('Edit Test Type');
			}
		});
	}
	function getDelData(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/test-type/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$('#myModal').find('.box-title').html('Delete Test Type');
			}
		});
	}

	function show(d) {
		result = '';
		$(d).each(array);
		$('#testTypeList').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/test-type/',
			success : function(d) {
				show(d)
			}
		});
	}
	//edit dalam kemungkinan nama terhapus
	$("#myModal").on('submit','#formEdit',function() {
				var namevalidation = $("#myModal").find('#formEdit').find('#edit-name').val();
				if (namevalidation == "") {
					alert('Name must be filled');
					return false;
				} else {
					var dataformatnyajson = getFormData($("#myModal").find('#formEdit'));
					$.ajax({
						type : 'put',
						url : contextName + '/api/test-type/',
						data : JSON.stringify(dataformatnyajson),
						dataType : 'json',
						contentType : 'application/json',
						success : function(d) {
							$('#myModal').modal('hide');
							alert('Data Successfully Updated');
							loadData();
						},
						error : function(d) {
							alert('gagal');
						}
					});
					return false;
				}
			});
	//submit akan selalu meminta redirect dari halaman membawa datanya
	$("#myModal").on('submit','#formgue',function() {
				var namevalidation = $("#myModal").find('#formgue').find(
						'#add-name').val();
				if (namevalidation == "") {
					alert('Name must be filled');
					return false;
				} else {
					var dataformatnyajson = getFormData($('#myModal').find(
							'#formgue'));
					/* alert(dataformatnyajson.name); */

					$.ajax({
						type : 'post',
						url : contextName + '/api/test-type/',
						data : JSON.stringify(dataformatnyajson),
						dataType : 'json',
						contentType : 'application/json',
						success : function(d) {
							loadData();

							$('#myModal').modal('hide');
							alert('Data Successfully Added');
						},
						error : function(d) {
							alert('gagal');
						}
					});
					//tidak langsung di redirect oleh submit tapi jadi diproses melalui ajax di atas
					return false;
				}
			});

	/* 	function simpanData() {
	 var dataformatnyajson = getFormData($('#formgue'));
	
	 $.ajax({
	 type : 'post',
	 url : contextName + '/api/test-type/',
	 data : JSON.stringify(dataformatnyajson),
	 dataType : 'json',
	 contentType : 'application/json',
	 success : function(d) {
	 loadData();

	 $('#myModal').modal('hide');
	 alert('Data Successfully Added');
	 },
	 error : function(d) {
	 alert('gagal');
	 }
	 });

	 } */

	function deleteData() {
		var dataformatjson = getFormData($('#formDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/test-type/delete',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data Succesfully Deleted');
				loadData();
			},
			error : function(d) {
				alert('gagal');
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});
</script>