<form role="form" id="delete-monitoring">
	<div class="box-body">
		<div class="box-body">
			<input type="hidden" class="form-control" name="id" id="pm-id"
				value="${monitoring.id}" /> <input type="hidden"
				class="form-control" name="biodataId" id="del-biodataId"
				value="${monitoring.biodata.id}" /> <input type="hidden"
				class="form-control" name="idleDate" id="del-idleDate"
				value="${monitoring.idleDate}" /> <input type="hidden"
				class="form-control" name="lastProject" id="del-lastProject"
				value="${monitoring.lastProject}" /> <input type="hidden"
				class="form-control" name="idleReason" id="del-idleReason"
				value="${monitoring.idleReason}" /> <input type="hidden"
				class="form-control" name="createdBy" id="del-createdBy"
				value="${monitoring.createdBy}" /> <input type="hidden"
				class="form-control" name="createdOn" id="del-createdOn"
				value="${monitoring.createdOn}" /> <input type="hidden"
				class="form-control" name="modifiedBy" id="del-modifiedBy"
				value="${monitoring.modifiedBy}" /> <input type="hidden"
				class="form-control" name="modifiedOn" id="del-modifiedOn"
				value="${monitoring.modifiedOn}" /> <input type="hidden"
				class="form-control" name="isDelete" id="del-isDelete"
				value="${monitoring.isDelete}" /> <input type="hidden"
				class="form-control" name="placementDate" id="del-placementDate"
				value="${monitoring.placementDate}" /> <input type="hidden"
				class="form-control" name="placementAt" id="del-placementAt"
				value="${monitoring.placementAt}" /> <input type="hidden"
				class="form-control" name="notes" id="del-notes"
				value="${monitoring.notes}" />
		</div>
		<div class="form-group">
			<label><h1>Apakah anda yakin menghapus data?</h1></label>
		</div>
	</div>
	
	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="delData()" data-dismiss="modal">Yes</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
	</div>
</form>