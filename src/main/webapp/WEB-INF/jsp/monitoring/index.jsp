<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">IDLE MONITORING</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search"
						placeholder="Search by name / majors" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>IDLE DATE</th>
					<th>PLACEMENT DATE</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="list-monitoring">
				<c:forEach items="${listMonitoring}" var="monitoring">
					<tr>
						<td>${monitoring.biodata.name}</td>
						<td>${monitoring.idleDate}</td>
						<td>${monitoring.placementDate}</td>
						<td><div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${monitoring.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataPlacement(${monitoring.id})">Placement</a>
									<li><a href="#" onclick="delDataGet(${monitoring.id})">Delete</a>
									</li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function(){
		$.ajax({
			url : contextName + '/monitoring/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data){
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').find('.box-title').html('Input Idle')
			}
		});
	});
	
	$('#modal-form').on('submit','#add-monitoring',function(){
		var dataformatjson = getFormData($('#add-monitoring'))
		$.ajax({
			type : 'post',
			url : contextName + '/api/monitoring/post/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data){
				alert('Data successfully saved!');
				$('#modal-form').modal('hide');
				loadData();
			},
			error : function(data){
				alert('Failed')
			}
		});
		return false;
	});
	
	function loadData(){
		$.ajax({
			url : contextName + '/monitoring/list.html',
			type : 'get',
			success : function(data){
				$('#list-monitoring').html(data);
			}
			
		});
	}
	
	function loadDataGet(id){
		$.ajax({
			url : contextName + '/monitoring/edit.html',
			type : 'get',
			data : {"id":id},
			success : function(data){
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function updateMonitoring(){
		var dataformatnyajson = getFormData($('#edit-monitoring'));
		$.ajax({
			url : contextName + '/api/monitoring/update/',
			type : 'put',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data){
				alert('Idle successfully updated!');
				$('#modal-form').modal('hide');
				loadData();
			}
		});
	}
	
	function loadDataPlacement(id){
		$.ajax({
			url : contextName + '/monitoring/placement.html',
			type : 'get',
			data : {"id":id},
			success : function(data){
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	$('#modal-form').on('submit','#placement',function(){
		var placementDateVal = $('#mofal-form').find('#placement').find('#placementDate').val();
		var placementAtVal = $('#mofal-form').find('#placement').find('#placementAt').val();
		if(placementDateVal==""){
			alert('Placement date must be filled!');
			return false;
		} else if (placementAtVal==""){
			alert('Placement at must be filled!');
			return false;
		} else {
			var dataformatnyajson = getFormData($('#placement'));
			$.ajax({
				url : contextName + '/api/monitoring/placement/',
				type : 'put',
				data : JSON.stringify(dataformatnyajson),
				dataType : 'json',
				contentType : 'application/json',
				success : function(data){
					alert('Placement successfully added!');
					$('#modal-form').modal('hide');
					loadData();
				}
			});
			return false;
		}
	});
	
	function delDataGet(id){
		$.ajax({
			url : contextName + '/monitoring/delete.html',
			type : 'get',
			data : {"id":id},
			success : function(data){
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function delData(){
		var formatnyajson = getFormData($('#delete-monitoring'));
		$.ajax({
			url : contextName + '/api/monitoring/delete/',
			type : 'put',
			data : JSON.stringify(formatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data){
				alert('Data successfully deleted!');
				$('#modal-form').modal('hide');
				loadData();
			}
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/monitoring/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#list-monitoring').html(data);
			}
		});
	});
	
</script>