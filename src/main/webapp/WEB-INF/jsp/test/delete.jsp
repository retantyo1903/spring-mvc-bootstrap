<form id="test-delete" class="form-horizontal pull-center">
	<input type="hidden" class="form-control" name="id" id="dId"
		value="${test.id}"> 
	<input type="hidden" class="form-control" name="createdBy" id="dCreatedBy" 
		value="${test.createdBy}"> 
	<input type="hidden" class="form-control" name="createdOn" id="dCreatedOn"
		value="${test.createdOn}"> 
	<input type="hidden" class="form-control" name="name" id="dName" value="${test.name}">
	<input type="hidden" class="form-control" name="isBootcampTest" id="dBoot" value="${test.isBootcampTest}">
	<center>
		<h3>Are You Sure Want To Delete "${test.name}" ?</h3>
	</center>
	<div class="box-footer">
		<center>
			<button type="button" class="btn btn-warning" onclick="deleteData()"
				data-dismiss="modal">Yes</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
		</center>
	</div>
</form>