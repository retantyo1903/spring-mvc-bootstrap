<form id="test-add" class="form-horizontal pull-center">
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="name" id="iName"
				placeholder="Name">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<label>Is Bootcamp Test &emsp;&emsp;</label>
			<div class="radio">
				<label> <input type="radio" name="isBootcampTest" id="iYes"
					value="1"> Yes &emsp;
				</label> <label> <input type="radio" name="isBootcampTest" id="iNo"
					value="0"> No &emsp;
				</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea name="notes" id="iNotes" class="form-control" rows="3"
			placeholder="Notes"></textarea>
		</div>
	</div>

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="saveData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>