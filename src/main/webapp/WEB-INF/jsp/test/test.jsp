<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">TEST</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control"
						id="txt-search" placeholder="Search by Name" /> <span
						class="input-group-btn">
						<button class="btn btn-warning btn-flat" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>

			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="row"></div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CREATED BY</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="test-list">
				<c:forEach items="${testList}" var="test">
					<tr>
						<td>${test.name}</td>
						<td>${test.user.username}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${test.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataDelete(${test.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function() {
		$.ajax({
			url : contextName + "/test/add.html",
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-form").find(".box-body").html(data);
				$("#modal-form").find(".box-title").html('TEST');
				$("#modal-form").modal('show');
			}
		});
	});
	
	function loadData(){
		$.ajax({
			type : 'get',
			url : contextName + '/test/list.html',
			success : function(data){
				$('#test-list').html(data);
			}
		});
	}
	
	function saveData() {
		var json = getFormData($('#test-add'));

		$.ajax({
			url : contextName + '/api/test/post/',
			type : 'post',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully saved !');
				$('#modal-form').modal('hide');
				loadData();
			},
			error : function(d) {
				alert('Failed to save data !');
			}
		});
	}
	
	function loadDataGet(id) {
		$.ajax({
			url: contextName + '/test/edit.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$("#modal-form").find(".box-title").html('TEST');
				$('#modal-form').modal('show');
			}
		});
	}
	
	function updateData() {
		var json = getFormData($('#test-edit'));
		$.ajax({
			url: contextName + '/api/test/update/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert('Data successfully updated !');
				$('#modal-form').modal('hide');
				loadData();
			},
			error: function(d) {
				alert('Failed to update data !');
			}
			
		});
	}
	
	function loadDataDelete(id) {
		$.ajax({
			url: contextName + '/test/delete.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function deleteData() {
		var json = getFormData($('#test-delete'));
		$.ajax({
			url: contextName + '/api/test/delete/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert('Data successfully deleted');
				$('#modal-form').modal('hide');
				loadData();
			},
			error: function(d) {
				alert('Failed to delete data !');
			}
			
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url: contextName + "/test/search.html",
			type: 'get',
			data: {search: $('#txt-search').val()},
			dataType: 'html',
			success: function(data) {
				$('#test-list').html(data);
			}
		});
	})
</script>