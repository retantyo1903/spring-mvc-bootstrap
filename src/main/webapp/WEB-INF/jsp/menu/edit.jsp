<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="editMenu">

	<div class="form-group">
	<input type="hidden" class="form-control" name="id" id="edit-id" value="${menu.id}">
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy"	value="${menu.createdBy}"> 
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${menu.createdOn}">
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${menu.isDelete}">
	</div>
	<div class="form-group">
		<input type="text" name="code" id="edit-menu" class="form-control" value="${menu.code}" placeholder="Code"/>
	</div>
	<div class="form-group">
		<input type="text" name="title" id="edit-title" class="form-control" value="${menu.title}" placeholder="Title"/>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="description" id="edit-description"
			placeholder="Description">${menu.description}</textarea>
	</div>
	<div class="form-group">
		<input type="text" name="imageUrl" id="edit-imageUrl" class="form-control" value="${menu.imageUrl}" placeholder="Image URL"/>
	</div>
	<div class="form-group">
		<input type="text" name="menuOrder" id="edit-menuOrder" class="form-control" value="${menu.menuOrder}" placeholder="Menu Order"/>
	</div>
	<div class="form-group">
			<select class="form-control" style="width: 100%;" name="menuParent">
				<option value="" disabled selected>Menu Parent</option>
					<option value="${menu.id}">${menu.title}</option>		
			</select>
	</div>
	<div class="form-group">
		<input type="text" name="menuUrl" id="edit-menuUrl" class="form-control" value="${menu.menuUrl}" placeholder="Menu URL"/>
	</div>
	
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="updateData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
