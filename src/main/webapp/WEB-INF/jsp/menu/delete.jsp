	<form role="form" id="menuDelete">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="delete-id" value="${menu.id}"> 
			<input type="hidden" class="form-control" name="code" id="delete-code" value="${menu.code}">
			<input type="hidden" class="form-control" name="title" id="delete-title" value="${menu.title}">
			<input type="hidden" class="form-control" name="description" id="delete-description" value="${menu.description}">
			<input type="hidden" class="form-control" name="imageUrl" id="delete-imageUrl" value="${menu.imageUrl}">
			<input type="hidden" class="form-control" name="menuOrder" id="delete-menuOrder" value="${menu.menuOrder}">
			<input type="hidden" class="form-control" name="menuParent" id="delete-menuParent" value="${menu.menuParent}">
			<input type="hidden" class="form-control" name="menuUrl" id="delete-menuUrl" value="${menu.menuUrl}">
			<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${menu.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${menu.createdOn}"> 
			<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${menu.isDelete}"> 
		</div>
		<div class ="form-group">
		<label>Yakin mau dihapus?</label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Oke</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
