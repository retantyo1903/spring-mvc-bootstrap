<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">TECHNOLOGY</h3>
	</div>

	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" name="search-box" id="txt-search"
						placeholder="Search by name" /> <span
						class="input-group-btn">
						<button class="btn btn-warning btn-flat" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>

			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="row"></div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CREATED BY</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="ttrList">
				<c:forEach items="${list}" var="ttr">
					<tr>
						<td>${ttr.trainer.name}</td>
						<td>${ttr.user.username}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${ttr.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataDelete(${ttr.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function() {
		$.ajax({
			url : contextName + "/technologyTrainer/add.html",
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-form").find(".box-body").html(data);
				$("#modal-form").find(".box-title").html('TECHNOLOY TRAINER');
				$("#modal-form").modal('show');
			}
		});
	});

	function saveData() {
		var json = getFormData($('#batch-add'));

		$.ajax({
			url : contextName + '/api/batch/post/',
			type : 'post',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				$('#modal-form').modal('hide');
			}
		});
	}
	
	function loadDataGet(id) {
		$.ajax({
			url: contextName + '/batch/edit.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function updateData() {
		var json = getFormData($('#batch-edit'));
		$.ajax({
			url: contextName + '/api/batch/update/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				$('#modal-form').modal('hide');
			}
			
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url: contextName + "/batch/search.html",
			type: 'get',
			data: {search: $('#txt-search').val()},
			dataType: 'html',
			success: function(data) {
				$('#batch-list').html(data);
			}
		});
	})
	
	function show(data){
		result = '';
		$(data).each(array);
		$('#ttrlist').html(result);
	}
	
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/trainer/isi.html',
			success : function(d) {
				$('#ttrlist').html(d);
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});
</script>