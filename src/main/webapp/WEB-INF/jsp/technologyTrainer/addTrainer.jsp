<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box-body-trainer">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="trainerId">
				<option value="" disabled selected>Trainer</option>
				<c:forEach items="${listTtr}" var="Ttr">
					<option value="${Ttr.trainer.id}">${Ttr.trainer.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>

	<div class="box-footer">
		<button type="button" class="btn btn-warning"
			onclick="submitAddAnak()" data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</div>
<script>
	$('.select2').select2()
</script>
