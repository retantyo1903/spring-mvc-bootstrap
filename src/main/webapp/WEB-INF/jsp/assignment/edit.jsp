<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="assignEdit" class="form" role="form">
<div class="box-body">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="edit-id" value="${assignment.id}">
			<input type="hidden" class="form-control" name="testId" id="edit-testId" value="${assignment.testId}">
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy"	value="${assignment.createdBy}"> 
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${assignment.createdOn}">
			<input type="hidden" class="form-control" name="isHold" id="edit-isHold" value="${assignment.isHold}"> 
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${assignment.isDelete}">
		</div>
	</div>
	<div>
		<div class="form-group">
			<select class="form-control" style="width: 100%;" name="testId">
				<option value="" disabled selected>${assignment.biodata.name}</option>
				<c:forEach items="${bioList}" var="bio">
					<option value="${bio.id}">${bio.name}</option>				
				</c:forEach>
			</select>
	</div>
	<div class="form-group">
		<input type="text" name="title" id="edit-title" class="form-control" value="${assignment.title}"" placeholder="Title"/>
	</div>
	<div class="form-group">
			<div class="input-group date">
				<input type="date" class="form-control pull-left"
					name="startDate" value="${assignment.startDate}" id="edit-startDate" " placeholder="Start Date" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
	</div>
	<div class="form-group">
			<div class="input-group date">
				<input type="date" class="form-control pull-left"
					name="endDate" value="${assignment.endDate}" id="edit-endDate" " placeholder="End Date" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="description" " id="edit-description"
			placeholder="Description">${assignment.description}</textarea>
	</div>
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
	</div>
</form>
<script>
	$('.datepicker').datepicker({
		autoclose : true
	})

	$('.select2').select2()
</script>

<style>
.opt-title {
	color: grey;
}
</style>