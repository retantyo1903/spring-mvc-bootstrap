<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${assignList}" var="assig">
	<tr>
		<td>${assig.biodata.name}</td>
		<td>${assig.startDate}</td>
		<td>${assig.endDate}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
						<li><a href="#" onclick="loadDataGet(${assig.id})">Edit</a>
									</li>
									<li><a href="#" onclick="delDataGet(${assig.id})">Delete</a>
									</li>
									<li><a href="#">Hold</a>
									</li>
									<li><a href="#" onclick="markDataGet(${assig.id})">Mark as Done</a>
									</li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>