<form role="form" id="questionEdit">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="id" id="id" value="${question.id}">
			</div>
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="question"
					id="questionE" value="${question.question}">
			</div>
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="questionType"
					id="questionTypeE" value="${question.questionType}">
			</div>
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="createdBy"
					id="createdBy" value="${question.createdBy}">
			</div>
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="createdOn"
					id="createdOn" value="${question.createdOn}">
			</div>
			<div class="col-xs-2">
				<input type="hidden" class="form-control" name="isDelete"
					id="isDelete" value="${question.isDelete}">
			</div>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="optionA" id="optionA"
				placeholder="A" value="${question.optionA}">
		</div>

		<div class="form-group">
			<input type="text" class="form-control" name="optionB" id="optionB"
				placeholder="B" value="${question.optionB}">
		</div>

		<div class="form-group">
			<input type="text" class="form-control" name="optionC" id="optionC"
				placeholder="C" value="${question.optionC}">
		</div>

		<div class="form-group">
			<input type="text" class="form-control" name="optionD" id="optionD"
				placeholder="D" value="${question.optionD}">
		</div>

		<div class="form-group">
			<input type="text" class="form-control" name="optionE" id="optionE"
				placeholder="E" value="${question.optionE}">
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	</div>
</form>
