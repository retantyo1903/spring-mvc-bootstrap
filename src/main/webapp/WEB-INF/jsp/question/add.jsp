<form role="form" id="questionInput">
	<div class="box-body">
		<div class="form-group">
			<select name="questionType" class="form-control" id="questionType">
				<option value="">--Choose--</option>
				<option value="MC">Multiple Choice</option>
				<option value="ES">Essay</option>
			</select>
		</div>
		
		<div class="form-group">
			<textarea name="question" id="question" class="form-control" rows="3"
				placeholder="Question"></textarea>
		</div>
		
		<div class="form-group">
			<div class="input-group">
				<div class="custom-file">
					<input type="text" class="form-control" placeholder="upload image" name="imageUrl"
						id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"/>
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-flat">BROWSE</button>
					</span>	
				</div>
			</div>
		</div>
		
	</div>
	<div class="box-footer" align="right">
		<button type="button" class="btn btn-success" onclick="saveData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	</div>
</form>
