<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>

<div class="box">
	<div class="box-header">
		<h3 class="box-title">${judul}</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="col-xs-11">
			<form role="form">
				<div class="col-xs-4">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search By Question"> <span
								class="input-group-btn">
								<button type="button" class="btn btn-info btn-flat">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-xs-1">
			<button type="button" class="btn btn-warning" id="btn-pencet">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>
		<div></div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>QUESTION</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="questionList" align="center">
			</tbody>
		</table>
	</div>
</div>
<!-- MODAL BEGIN -->
<div id="questionModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>
<!-- MODAL END -->



<script>
	//add 
	$("#btn-pencet").click(function() {
		alert("kepencet");
		
		$.ajax({
			url : '${contextName}/question/add.html',
			type : 'GET',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$('#questionModal').find('.modal-body').html(data);
				$('#questionModal').find('.modal-title').html("ADD QUESTION");
				//tampilkan modal
				$('#questionModal').modal("show");
			}
		});
	});
	function saveData() {
		var dataformatjson = getFormData($('#questionInput'));
		$.ajax({
			type : 'post',
			url : contextName + '/api/question/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
				$('#questionModal').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});
	}
	//list
	var result = '';
	function clearForm() {
		$('#question').val("");
		$('#questionType').val("");
		$('#inputGroupFile01').val("");
		result='';
	}
	function array(i, e) {
		result += '<tr>';
		result += '<td>' + e.question + '</td>';
		result += '<td>'
					 +'<div class="btn-group">'
						+'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'
							+'<i class="glyphicon glyphicon-menu-hamburger"></i>' 
						+'</button>'
						+'<ul class="dropdown-menu">';
						if(e.questionType=='MC'){
							result += '<li><a href="#" data-toggle="modal" onclick="loadDataGet('+ e.id+ ')">Set Choices</a></li>';
							result += '<li><a href="#" onClick="loadDataDelete('+e.id+')">Delete</a></li>';
						}else{
							result += '<li><a href="#" onClick="loadDataDelete('+e.id+')">Delete</a></li>';
						}
			result +=    '</ul>'
					 +'</div>'
					+'</td>';
		result += '</tr>';
	}

	function show(d) {
		result = '';
		$(d).each(array);
		$('#questionList').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/question/',
			success : function(d) {
				show(d);
			}
		});
	}
	//edit
	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/question/edit.html',
			data : {"id" : id},
			success : function(d) {
				//mengisi data html
				$('#questionModal').find('.modal-body').html(d);
				$('#questionModal').find('.modal-title').html("SET CHOICE");
				//tampilkan modal
				$("#questionModal").modal("show");
			}
		});
	}

	function updateData() {
		var dataformatjson = getFormData($('#questionEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/choice/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			}
		});
	}
	//delete
	function loadDataDelete(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/question/delete.html',
			data : {"id":id},
			success : function(data) {
				$('#questionModal').find('.modal-body').html(data);
				$('#questionModal').modal('show');
			}
		});
	}
	function deleteData() {
		var dataformatjson = getFormData($('#questionDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/question/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			}
		});
	}
</script>

<script>
	$(document).ready(function() {
		loadData();
	});
</script>