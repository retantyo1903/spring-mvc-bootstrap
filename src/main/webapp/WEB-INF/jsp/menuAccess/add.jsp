<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="addMenuAccess" class="form" role="form">
	<div class="box-body">
		<!-- select role -->
		<div class="form-group">
			<div>
			<label>Role</label>
				<select class="form-control select2" style="width: 100%"
					name="roleId" required>
					<option value="" disabled selected>Role</option>
					<c:forEach items="${rList}" var="role">
						<option value="${role.id}">${role.name}</option>
					</c:forEach>
				</select>
			</div>	
		</div>
		
		<!-- select menu -->
		<div class="form-group">
			<div>
			<label>Category</label>
				<select class="form-control select2" style="width: 100%"
					name="menuId" required>
					<option value="" disabled selected>Menu</option>
					<c:forEach items="${mList}" var="menu">
						<option value="${menu.id}">${menu.title}</option>
					</c:forEach>
				</select>
			</div>	
		</div>
		
		<div class="box-footer pull-right">
			<button type="submit" class="btn btn-warning" >Save</button>
			<button type="reset" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</form>