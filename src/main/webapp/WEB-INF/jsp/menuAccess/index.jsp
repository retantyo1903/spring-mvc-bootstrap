<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<!-- jstl untuk memasukkan data list menggunakan foreach -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">Menu Access</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
			 <div class="input-group">
			 	<select class="form-control select2" style="width: 100%"
					name="roleId" id="searchId">
					<option value="" disabled selected>--Select Role--</option>
					<c:forEach items="${rList}" var="role">
						<option value="${role.id}">${role.name}</option>
					</c:forEach>
				</select>
			 	<span class="input-group-btn">
			 		<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
			 		</span>
			 	</div>
			</div>
				<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>ROLE</th>
					<th>MENU</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:forEach items="${list}" var="menuAccess">
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		loadData();
	});
	//onclick btn-add prepares the form add.jsp into the modal
	$("#btn-add").click(function(){
		$.ajax({
			url: contextName+'/menu-access/add.html',
			type: 'get',
			dataType:'html',
			success: function(data){
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').find('.box-title').html('Add Menu Access');
			}
		});
	});
	//saves the data into the table through method post
	$("#modal-form").on('submit','#addMenuAccess',function() {
		var dataFormJson =getFormData($('#addMenuAccess'))
		$.ajax({
			url:contextName+'/api/menu-access/add/',
			type:'post',
			data:JSON.stringify(dataFormJson),
			dataType:'json',
			contentType:'application/json',
			success:function(data){
				$('#modal-form').modal('hide');
				alert('Data Has Been Successfully Added!')
				loadData();
			},
			error:function(data){
				alert('failed')
			}
		});
		return false;
	});
	
	//get the data of the id taken from the button
	$('#list-data').on('click','.btn-delete',function(){
		$.ajax({
			url:contextName+'/menu-access/delete.html',
			type:'get',
			dataType:'html',
			data:{'id':$(this).attr('data')},
			success: function(deleteMA){
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(deleteMA);
				$('#modal-form').find('.box-title').html('Delete Menu Access');
			}
		});
	});
	
	function deleteAccess(id){
		$.ajax({
			type:'delete',
			url:contextName+'/api/menu-access/delete/'+id,
			success:function(deletedAccess){
				alert('Access Successfully Deleted')
				loadData();
			},
			error:function(deletedAccess){
				alert('Failed to delete access')
			}
		});
	}
	
	function loadData(){
		$.ajax({
			type: 'get',
			url: contextName+'/menu-access/list.html',
			success: function(data){
				$('#list-data').html(data);
			}
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/menu-access/search.html',
			data : {cari : $('#searchId').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#list-data').html(data);
			}
		});
	});
</script>