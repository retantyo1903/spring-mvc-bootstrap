<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="menuAccess">
	<tr>
		<td>${menuAccess.role.name}</td>
		<td>${menuAccess.menu.title}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
				<li><a class="btn btn-delete btn-sm" data="${menuAccess.id}"><i class="fa fa-exclamation-circle"></i>Delete</a></li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>

