<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form id="editAjah" class="form-horizontal">
	<input type="hidden" id="oe-id" name="id" class="form-control" value="${office.id}"/>
	<input type="hidden" id="oe-createdBy" name="createdBy" class="form-control" value="${office.createdBy}"/>
	<input type="hidden" id="oe-createdOn" name="createdOn" class="form-control" value="${office.createdOn}"/>
	<input type="hidden" id="oe-isDelete" name="isDelete" class="form-control" value="${office.isDelete}"/>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oe-name" name="name" class="form-control"
				placeholder="Name" value="${office.name}"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oe-phone" name="phone" class="form-control"
				placeholder="Phone" value="${office.phone}" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oe-email" name="email" class="form-control"
				placeholder="Email" value="${office.email}" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oe-address" name="address"
				class="form-control" placeholder="Address" value="${office.address}" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea id="ca-notes" name="notes" class="form-control"
				placeholder="Description">${office.notes}</textarea>
		</div>
	</div>
	<table class="table table-responsive">
		<thead>
			<tr>
				<td>CODE</td>
				<td>NAME</td>
				<td>CAPACITY</td>
				<td></td>
			</tr>
		</thead>
		<tbody id="isiRoom">
			<%-- <c:forEach>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown">
								<i class="glyphicon glyphicon-menu-hamburger"></i>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#" onclick="loadData('${category.id}')">Edit</a></li>
								<li><a href="#" onclick="delDataGet('+ e.id +')">Delete</a></li>
							</ul>
						</div>
					</td>
				</tr>
			</c:forEach> --%>
		</tbody>
	</table>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="updateE()">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>