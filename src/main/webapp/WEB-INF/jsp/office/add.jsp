<form id="addAjah" class="form-horizontal">
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oa-name" name="name" class="form-control"
				placeholder="Name" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oa-phone" name="phone" class="form-control"
				placeholder="Phone" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oa-email" name="email" class="form-control"
				placeholder="Email" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="oa-address" name="address"
				class="form-control" placeholder="Address" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea id="ca-notes" name="notes" class="form-control"
				placeholder="Description"></textarea>
		</div>
	</div>
	<div class="form-goup">
		<button class="btn btn-warning" onClick="loadRoom()">+ ROOM</button>
	</div>
	<table class="table table-responsive">
		<thead>
			<tr>
				<td>CODE</td>
				<td>NAME</td>
				<td>CAPACITY</td>
				<td></td>
			</tr>
		</thead>
		<tbody id="isiRoom">
			
		</tbody>
	</table>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="simpan()" data-dismiss="modal">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>