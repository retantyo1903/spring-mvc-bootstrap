<form role="form" id="testimonyEdit">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="id" id="edit-id" value="${testimony.id}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdBy"
					id="edit-createdBy" value="${testimony.createdBy}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdOn"
					id="edit-createdOn" value="${testimony.createdOn}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="isDelete"
					id="edit-isDelete" value="${testimony.isDelete}">
			</div>
		</div>
		<div class="form-group">
			<label>Title</label> <input type="text" class="form-control"
				name="title" id="edit-title" value="${testimony.title}">
		</div>

		<div class="form-group">
			<label>Content</label>
			<textarea class="form-control" name="content" id="edit-content">${testimony.content}</textarea>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="updateData()" data-dismiss="modal">Save</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
