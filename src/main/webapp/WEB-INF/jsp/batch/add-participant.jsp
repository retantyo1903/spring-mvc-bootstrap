<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="participant-add" class="form-horizontal pull-center">
	<input type="hidden" class="form-control" name="batchId" value="${batch.id}" />
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="biodataId">
				<option value="" disabled selected>Add Participant</option>
				<c:forEach items="${biodataList}" var="biodata">
					<option value="${biodata.id}">${biodata.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>

	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="saveParticipant()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>
	$('.select2').select2()
</script>

<style>
.opt-title {
	color: grey;
}
</style>