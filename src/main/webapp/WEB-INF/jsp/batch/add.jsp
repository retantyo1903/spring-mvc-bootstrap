<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="batch-add" class="form-horizontal pull-center">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="technologyId">
				<option value="" disabled selected>Technology</option>
				<c:forEach items="${techList}" var="tech">
					<option value="${tech.id}">${tech.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="trainerId">
				<option value="" disabled selected>Trainer</option>
				<c:forEach items="${trainerList}" var="trainer">
					<option value="${trainer.id}">${trainer.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="name"
				placeholder="Name" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" class="form-control pull-left"
					name="periodFrom" placeholder="Period from" onfocus="(this.type='date')" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" class="form-control pull-left"
					name="periodTo" placeholder="Period to" onfocus="(this.type='date')" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="roomId">
				<option value="" disabled selected>Room</option>
				<c:forEach items="${roomList}" var="room">
					<option value="${room.id}">${room.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="bootcampTypeId">
				<option value="" disabled selected>Bootcamp Type</option>
				<c:forEach items="${bootList}" var="boot">
					<option value="${boot.id}">${boot.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea class="form-control" rows="3" name="notes"
				placeholder="Notes"></textarea>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="saveData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>
	$('.select2').select2()
</script>

<style>
.opt-title {
	color: grey;
}
</style>