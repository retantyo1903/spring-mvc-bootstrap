<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="batch">
					<tr>
						<td>${batch.technology.name}</td>
						<td>${batch.name}</td>
						<td>${batch.trainer.name}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${batch.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataDelete(${batch.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>