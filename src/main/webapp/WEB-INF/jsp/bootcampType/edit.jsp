<form id="bootcampTypeEdit">
<div class="box-body">
	<input type="hidden" class="form-control" name="id" id="edit-id" value="${bootcampType.id}">
	<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy" value="${bootcampType.createdBy}">
	<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${bootcampType.createdOn}">
	<input type="hidden" class="form-control" name="modifiedOn" id="edit-modifiedOn" value="${bootcampType.modifiedOn}">
	<input type="hidden" class="form-control" name="modifiedBy" id="edit-modifiedBy" value="${bootcampType.modifiedBy}">
	<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${bootcampType.isDelete}">	

	<div class="form-group">
			<div class="form-group">
			<label>Name</label> <input class="form-control" name="name" id="edit-name" value="${bootcampType.name}">
			</div>
			<div class="form-group">
			<label>Notes</label><textarea class="form-control" name="notes" id="edit-notes">${bootcampType.notes}</textarea>
			</div>
		</div>
	
	<div class="box-footer pull-right" >
		<button type="submit" class="btn btn-warning" >Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</div>
</form>