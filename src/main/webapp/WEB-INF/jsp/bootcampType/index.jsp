
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<!-- jstl untuk memasukkan data list menggunakan foreach -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">Bootcamp Type</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="searchBar"
						placeholder="Search by name" /> <span class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CREATED BY</th>
					<th>STATUS</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:forEach items="${list}" var="bootcampType">
					<tr>
						<td><p></p></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<!-- Modal  -->
<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>
<!-- modal end -->
<script>
	//base practice agar code tidak jalan sebelum page siap
	$(document).ready(function() {
		loadData();
	});
	$("#btn-add").click(function() {
		$.ajax({
			url : "${contextName}/bootcamp-type/add.html",
			type : "GET",
			dataType : "html",
			success : function(anuku) {
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(anuku);
				$('#modal-form').find('.box-title').html('Add Bootcamp Type');
			}
		});
	});
	$("#list-data").on('click', '.btn-edit', function() {
		$.ajax({
			url : "${contextName}/bootcamp-type/edit.html",
			type : "GET",
			dataType : "html",
			data : {
				'id' : $(this).attr('data')
			},
			success : function(anuku) {
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(anuku);
				$('#modal-form').find('.box-title').html('Edit Bootcamp Type');
			}
		});
	});
	$("#list-data").on(
			'click',
			'.btn-delete',
			function() {
				$.ajax({
					url : "${contextName}/bootcamp-type/delete.html",
					type : "get",
					dataType : "html",
					data : {
						'id' : $(this).attr('data')
					},
					success : function(datadel) {
						$('#modal-form').modal('show');
						$('#modal-form').find('.box-body').html(datadel);
						$('#modal-form').find('.box-title').html(
								'Delete Bootcamp Type')
					}
				});
			});

	function deleteData() {
		var dataFormatnyaJson = getFormData($('#bootcampTypeDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/bootcamp-type/delete',
			data : JSON.stringify(dataFormatnyaJson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(dataDel) {
				alert('Bootcamp Type succesfully Non-Activated');
				loadData();
			},
			error : function(dataDel) {
				alert('Failed');
			}
		});
	}
	//submit edit
	$("#modal-form").on('submit','#bootcampTypeEdit',function() {
				var namevalidation = $("#modal-form").find('#bootcampTypeEdit')
						.find('#edit-name').val();
				if (namevalidation == "") {
					alert('Name must be filled');
					return false;
				} else {
					var dataFormatnyaJson = getFormData($("#modal-form").find(
							'#bootcampTypeEdit'));
					$.ajax({
						type : 'put',
						url : contextName + '/api/bootcamp-type/',
						data : JSON.stringify(dataFormatnyaJson),
						dataType : 'json',
						contentType : 'application/json',
						success : function(d) {
							alert('Data Succesfully Updated!');
							$('#modal-form').modal('hide');
							loadData();
						},
						error : function(d) {
							alert('FAILED')
						}
					});
					return false
				}
			});
	//submit
	$("#modal-form").on('submit','#bootcampTypeAdd',function() {
				var namevalidation = $("#modal-form").find('#bootcampTypeAdd')
						.find('#add-name').val();
				if (namevalidation == "") {
					alert('Name must be filled');
					return false;
				} else {

					var dataFormatnyaJson = getFormData($('#bootcampTypeAdd'))
					$.ajax({
						type : 'post',
						url : contextName + '/api/bootcamp-type/',
						data : JSON.stringify(dataFormatnyaJson),
						dataType : 'json',
						contentType : 'application/json',
						success : function(isi) {

							alert('Data successfully Saved!');
							$('#modal-form').modal('hide');
							loadData();
						},
						error : function(isi) {
							alert('Failed')
						}
					});
					return false;
				}
			});
	//mengisi table
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/bootcamp-type/list.html',
			success : function(data) {
				$('#list-data').html(data);
			}
		});
	}
	//search
	$("#btn-search").click(function() {
		$.ajax({
			url : contextName + '/bootcamp-type/search.html',
			data : {
				cari : $('#searchBar').val()
			},
			dataType : 'html',
			type : 'get',
			success : function(data) {
				$('#list-data').html(data);
			}
		});
	});
</script>