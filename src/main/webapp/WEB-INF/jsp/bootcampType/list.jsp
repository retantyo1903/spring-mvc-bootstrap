<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${list}" var="bootcampType">
	<tr>
		<td><p>${bootcampType.name}</p></td>
		<td>${bootcampType.user.username}</td>
		<td>
		<c:choose>
    		<c:when test="${bootcampType.isDelete=='0'}">
        ACTIVE
   		 </c:when>    
  			  <c:otherwise>
        NON ACTIVE
   			 </c:otherwise>
		</c:choose>
		</td>
		<td> 
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="glyphicon glyphicon-menu-hamburger"></i>
			</button>
			<ul class="dropdown-menu">
				<li><a class="btn btn-edit btn-sm" data="${bootcampType.id}"><i class="fa fa-edit"></i>Edit</a></li>
				<li><a class="btn btn-delete btn-sm" data="${bootcampType.id}"><i class="fa fa-exclamation-circle"></i>Delete</a></li>
			</ul>
			</div>
		</td>
	</tr>
</c:forEach>