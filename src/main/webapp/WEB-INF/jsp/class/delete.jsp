<form id="clazz-delete" class="form-horizontal pull-center">
	<input type="hidden" class="form-control" name="id" id="dId"
		value="${clazz.id}">
	<center>
		<h3>Are You Sure Want To Delete "${clazz.batch.name}" ?</h3>
	</center>
	<div class="box-footer">
		<center>
			<button type="button" class="btn btn-warning" onclick="deleteData(${clazz.id})"
				data-dismiss="modal">Yes</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
		</center>
	</div>
</form>