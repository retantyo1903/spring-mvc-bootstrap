<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${classList}" var="clazz">
	<tr>
		<td>${clazz.batch.name}</td>
		<td>${clazz.biodata.name}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="confirmDelete(${clazz.id})">Delete</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>