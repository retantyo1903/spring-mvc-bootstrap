<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">USER</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search"
						placeholder="Search by username" /> <span class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>USERNAME</th>
					<th>ROLE</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="userlist" align="left">
			</tbody>
		</table>
	</div>



</div>
<!-- Modal-begin -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title">USER</h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>
<!-- Modal-end -->


<script>
	function updateData() {
		var dataformatjson = getFormData($('#userEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/user/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			}
		});
	}

	function loadDataPass(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/user/reset.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});
		alert(id);
	}
	function updatePassword() {
		var dataformatjson = getFormData($('#reset'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/user/r',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			},
			error : function(d) {
				alert('gagal');
			}
		});
	}

	function delDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/user/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});
		alert(id);
	}

	function deleteData() {
		var dataformatjson = getFormData($('#userDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/user/d',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('terhapus');
				loadData();
			},
			error : function(d) {
				alert('gagal');
			}
		});
	}
	
	$("#btn-add").click(function() {
		
	
		 $.ajax({
			url : '${contextName}/user/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$('#myModal').find('.box-title').html('USER');
			}
		}); 
	});

	function closeModal() {
		$('#myModal').modal('hide');
	}
	/* function simpanData() {
		var dataformatnyajson = getFormData($('#formgue'));

		$.ajax({
			type : 'post',
			url : contextName + '/api/user/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();

				$('#myModal').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});

	} */
	
	$("#myModal").on('submit','#formgue', function() {
		var namevalidation = $("#myModal").find('#formgue').find('#add-username').val();
		if (namevalidation=="") {
			alert('name kosong');
			return false;
		} else {
			var dataformatnyajson = getFormData($('#myModal').find('#formgue'));

			$.ajax({
				type : 'post',
				url : contextName + '/api/user/',
				data : JSON.stringify(dataformatnyajson),
				dataType : 'json',
				contentType : 'application/json',
				success : function(d) {
					alert('Data successfully saved');
					loadData();
					
					$('#myModal').modal('hide');
					
				},
				error : function(d) {
					alert('Isi semua kolom');
				}
			});
				return false;
		}
	});

	var result = '';

	function array(i, e) {
		result += '<tr>';
		result += '<td>' + e.username + '</td>';
		result += '<td>' + e.roleId + '</td>';
		result += '<td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-menu-hamburger"></i></button><ul class="dropdown-menu"><li><a href="#" data-toggle="modal" data-target="#userEdit" onclick="loadDataGet('
				+ e.id
				+ ')">Edit</a></li><li><a href="#" data-toggle="modal" data-target="#reset" onclick="loadDataPass('
				+ e.id
				+ ')">Reset Password</a></li><li><a href="#" onclick="delDataGet('
				+ e.id + ')">Delete</a></li></ul></div></td>'
		result += '</tr>';
	}

	function show(d) {
		result = '';
		$(d).each(array);
		$('#userlist').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/user/',
			success : function(d) {
				show(d);

			}
		});
	}

	$(document).ready(function() {
		loadData();
	});

	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/user/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});
		alert(id);
	}
	 
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/user/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#userlist').html(data);
			}
		});
	});
</script>