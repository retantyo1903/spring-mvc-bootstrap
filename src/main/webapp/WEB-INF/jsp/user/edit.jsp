
<form id="userEdit">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="edit-id"
				value="${user.id}">
		</div>
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="createdBy"
				id="edit-createdBy" value="${user.createdBy}">
		</div>
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="createdOn"
				id="edit-createdOn" value="${user.createdOn}">
		</div>
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="password"
				id="edit-password" value="${user.password}">
		</div>
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="isDelete"
				id="edit-isDelete" value="${user.isDelete}">
		</div>
	</div>
	<div class="form-group">
	<select class="form-control" name=roleId id=edit-roleId value="${user.roleId}">
		<option value="1">Admin</option>
		<option value="2">Staff</option>
		<option value="3">User</option>
	</select>
	</div>
	<div class="form-group">
		<label>Username</label> <input class="form-control" name="username"
			placeholder="Username" id="edit-username" value="${user.username }"></>
	</div>
	<div class="form-group">
		<label>Mobile Flag Type &emsp;&emsp;</label>
		<div class="radio">
			<label> <input type="radio" name="mobileFlag" id="1"
				value="1"> True &emsp;
			</label> <label> <input type="radio" name="mobileFlag" id="0"
				value="0"> False &emsp;
			</label>
		</div>
	</div>
	<div class="form-group">
		<label><b>Mobile Token</b></label> <input class="form-control"
			name="mobileToken" placeholder="Mobile Token" id="edit-mobileToken" value="${user.mobileToken}"></>
	</div>
	<div class="box-footer pull-right">
	<button class="btn btn-warning" type="button" onclick="updateData()"
		data-dismiss="modal">Simpan</button>
	<button class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	
</form>