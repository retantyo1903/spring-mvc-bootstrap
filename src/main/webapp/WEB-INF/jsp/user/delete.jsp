	<form role="form" id="userDelete">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="delete-id" value="${user.id}"> 
			<input type="hidden" class="form-control" name="password" id="edit-password" value="${user.password}">
			<input type="hidden" class="form-control" name="username" id="edit-username" value="${user.username}">
			<input type="hidden" class="form-control" name="roleId" id="edit-roleId" value="${user.roleId}">
			<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${user.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${user.createdOn}"> 
			<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${user.isDelete}"> 

			<input type="hidden" class="form-control" name="mobileFlag" id="edit-mobileFlag" value="${user.mobileFlag}">
		</div>
		<div class ="form-group">
		<label>Yakin mau dihapus?</label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Oke</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
