
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">Biodata List</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search" placeholder="Search by name / majors" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>MAJORS</th>
					<th>GPA</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:forEach items="${list}" var="biodata">
					<tr>
						<td>${biodata.name}</td>
						<td>${biodata.majors}</td>
						<td>${biodata.gpa}</td>
						<td><div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${biodata.id})">Edit</a>
									</li>
									<li><a href="#" onclick="delDataGet(${biodata.id})">Delete</a>
									</li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function(){
		$.ajax({
			url : "${contextName}/biodata/add.html",
			type : "get",
			dataType : "html",
			success : function(data){
				$("#modal-form").modal('show');
				$("#modal-form").find('.box-body').html(data);
				$("#modal-form").find('.box-title').html('Add Biodata');
			}
		});
	});
	
	$("#modal-form").on('submit','#biodata-add',function(){
		var nameValidation = $('#modal-form').find('#biodata-add').find('#name').val();
		var lastEdValidation = $('#modal-form').find('#biodata-add').find('#lastEducation').val();
		var edLevelValidation = $('#modal-form').find('#biodata-add').find('#educationalLevel').val();
		var gradYearValidation = $('#modal-form').find('#biodata-add').find('#graduationYear').val();
		var majValidation = $('#modal-form').find('#biodata-add').find('#majors').val();
		var gpaValidation = $('#modal-form').find('#biodata-add').find('#gpa').val();
		if(nameValidation==""){
			alert('Name must be filled!');
			return false;
		} else if(lastEdValidation==""){
			alert('Last education must be filled!');
			return false;
		} else if(edLevelValidation==""){
			alert('Educational level must be filled!');
			return false;
		} else if(gradYearValidation==""){
			alert('Graduation Year must be filled!');
			return false;
		} else if(majValidation==""){
			alert('Majors must be filled!');
			return false;
		} else if(gpaValidation==""){
			alert('Majors must be filled!');
			return false;
		} else {
			var dataformatnyajson = getFormData($('#biodata-add'))
			$.ajax({
				type : 'post',
				url : contextName + '/api/biodata/',
				data : JSON.stringify(dataformatnyajson),
				dataType : 'json',
				contentType : 'application/json',
				success : function(data){
					alert('Data successfully saved!');
					$('#modal-form').modal('hide');
					loadData();
				},
				error : function(data){
					alert('Failed')
				}
			});
			return false;
		}
	})
	
	function loadData(){
		$.ajax({
			type : 'get',
			url : contextName + '/biodata/list.html',
			success : function(data){
				$('#list-data').html(data);
			}
		});
	}
	
	function loadDataGet(id){
		$.ajax({
			url : contextName + '/biodata/edit.html',
			type : 'get',
			data : {"id":id},
			success : function(data){
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function updateData() {
		var json = getFormData($('#biodata-edit'));
		$.ajax({
			url: contextName + '/api/biodata/update/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert("Data successfully updated!");
				$('#modal-form').modal('hide');
				loadData();
			}
			
		});
	}
	
	function delDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/biodata/delete.html',
			data : {"id" : id},
			success : function(data) {
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal("show");
			}
		});
	}

	function deleteData() {
		var dataformatjson = getFormData($('#delete-biodata'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/biodata/del/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert("Data successfully deleted!");
				$('#modal-form').modal('hide');
				loadData();
			}
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/biodata/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#list-data').html(data);
			}
		});
	});
</script>