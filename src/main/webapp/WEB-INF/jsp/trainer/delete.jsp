<form role="form" id="trainerDelete">
	<div class="box-body">

		<input type="hidden" class="form-control" name="id" id="delete-id"
			value="${trainer.id}"> <input type="hidden"
			class="form-control" name="createdBy" id="delete-createdBy"
			value="${trainer.createdBy}"> <input type="hidden"
			class="form-control" name="createdOn" id="delete-createdOn"
			value="${trainer.createdOn}"> <input type="hidden"
			class="form-control" name="isDelete" id="delete-isDelete"
			value="${trainer.isDelete}"> <input type="hidden"
			class="form-control" name="name" id="delete-name"
			value="${trainer.name}">
			<input type="hidden" class="form-control" name="notes" id="delete-notes" value="${trainer.notes}">
			


		<h3>Are You Sure Want To Delete Trainer ${trainer.name } ?</h3>

	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick=""
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
