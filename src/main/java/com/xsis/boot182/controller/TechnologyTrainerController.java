package com.xsis.boot182.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.DocumentTestDetail;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyTrainer;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.TechnologyService;
import com.xsis.boot182.service.TechnologyTrainerService;
import com.xsis.boot182.service.TrainerService;

@Controller
public class TechnologyTrainerController extends BaseController {

	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private TechnologyTrainerService technologyTrainerService;
	
	@Autowired
	private TechnologyService technologyService;
	
	@Autowired
	private TrainerService trainerService;

	@RequestMapping("/technologyTrainer.html")
	public String technology(Model model) {
		List<TechnologyTrainer> listTechnologyTrainer = new ArrayList<>();
		listTechnologyTrainer = this.technologyTrainerService.getList();
		model.addAttribute("list", listTechnologyTrainer);
		
		model.addAttribute("judul", "TECHNOLOGY TRAINER");
		return "technologyTrainer/list";
	}
	
	@RequestMapping("/technologyTrainer/isi.html")
	public String isi(Model model) {
		List<TechnologyTrainer> listTechnologyTrainer = new ArrayList<>();
		listTechnologyTrainer = this.technologyTrainerService.getList();
		model.addAttribute("list", listTechnologyTrainer);
		
		model.addAttribute("judul", "TECHNOLOGY TRAINER");
		return "technologyTrainer/isi";
	}
	
	@RequestMapping("/technologyTrainer/add.html")
	public String add(Model model) {
		List<Technology> listTech = new ArrayList<>();
		listTech = this.technologyService.getList();
		
		List<Trainer> listTrainer = new ArrayList<>();
		listTrainer = this.trainerService.getList();
		
		model.addAttribute("techList", listTech);
		model.addAttribute("trainerList", listTrainer);
		return "technologyTrainer/add";
	}
	
	@RequestMapping("/technology/addTrainer.html")
	public String addTrainer(Model model) {
		List<Trainer> listTtr = new ArrayList<>();
		listTtr = this.trainerService.getList();
		
		model.addAttribute("listTtr", listTtr);
		return "technology/addTrainer";
	}
	
	@RequestMapping(value="/api/technologyTrainer/{id}", method=RequestMethod.GET)
	public ResponseEntity<List<TechnologyTrainer>> get(@PathVariable(name="id") Long id) {
		ResponseEntity<List<TechnologyTrainer>> result = null;
		try {
			List<TechnologyTrainer> anak = this.technologyTrainerService.idTechnology(id);
			result = new ResponseEntity<List<TechnologyTrainer>> (anak, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value = "/api/technologyTrainer/", method = RequestMethod.POST)
	public ResponseEntity<TechnologyTrainer> save(@RequestBody TechnologyTrainer technologyTrainer) {
		ResponseEntity<TechnologyTrainer> result = null;
		try {
			this.technologyTrainerService.insert(technologyTrainer);
			result = new ResponseEntity<TechnologyTrainer>(technologyTrainer, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<TechnologyTrainer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}
