package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.model.User;
import com.xsis.boot182.service.UserService;

@Controller
public class UserController extends BaseController{
	private Log log= LogFactory.getLog(getClass());
	
	@Autowired
	private UserService userService;
	
	//mapping lokasi url
	@RequestMapping("/user.html")
	public String user(Model model) {
		return "user/user";
	}
	
	//mapping add
	@RequestMapping("/user/add.html")
	public String add(Model model) {
		return "user/add";
	}
	
	//mapping edit
	@RequestMapping("/user/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		User user = this.userService.getData(id);
		model.addAttribute("user", user);
		return "user/edit";
	}
	
	//mapping reset
	@RequestMapping("/user/reset.html")
	public String reset(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		User user = this.userService.getData(id);
		model.addAttribute("user", user);
		return "user/reset";
	}
	
	//mapping delete 
		@RequestMapping("/user/delete.html")
		public String delete(Model model, HttpServletRequest request) {
			Long id = Long.parseLong(request.getParameter("id"));
			User user = this.userService.getData(id);
			model.addAttribute("user", user);
			return "user/delete";
		}
		
	//mapping search
		@RequestMapping("/user/search.html")
		public String search(Model model, HttpServletRequest request) {
			String search = request.getParameter("cari");
			List<User> list = new ArrayList<>();
			list = this.userService.search(search);
			model.addAttribute("searchList", list);
			return "user/search";
		}
	
	//untuk get data berdasarkan database
	@RequestMapping(value="/api/user/", method=RequestMethod.GET)
	public ResponseEntity<List<User>> getAll() {
		ResponseEntity<List<User>> result=null;
		try {
			List<User> list = this.userService.getList();
			result= new ResponseEntity<List<User>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk menyimpan file baru/add user
	@RequestMapping(value="/api/user/", method=RequestMethod.POST)
	public ResponseEntity<User> save(@RequestBody User user) {
		ResponseEntity<User> result = null;
		try {
			//get date now
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			user.setCreatedOn(date);
			user.setCreatedBy(this.getUserId());
			user.setMobileFlag(this.getUserId());
			user.setIsDelete(0);
			this.userService.insert(user);
			result = new ResponseEntity<User>(user, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode update(edit)
	@RequestMapping(value="/api/user/", method=RequestMethod.PUT)
	public ResponseEntity<User> update(@RequestBody User user) {
		ResponseEntity<User> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			user.setModifiedOn(date);
			user.setModifiedBy(this.getUserId());
			this.userService.update(user);
			result = new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode get data by Id
	@RequestMapping(value="/api/user/{id}", method=RequestMethod.GET)
	public ResponseEntity<User> get(@PathVariable(name="id")Long id) {
		ResponseEntity<User> result = null;
		try {
			User user = this.userService.getData(id);
			result = new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	

	
	//untuk form delete/update isDelete
	@RequestMapping(value="/api/user/d", method=RequestMethod.PUT)
	public ResponseEntity<User> delete(@RequestBody User user) {
		ResponseEntity<User> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			user.setDeletedOn(date);
			user.setDeletedBy(this.getUserId());
			user.setIsDelete(1);
			this.userService.update(user);
			result = new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode update(reset password)
	@RequestMapping(value="/api/user/r", method=RequestMethod.PUT)
	public ResponseEntity<User> reset(@RequestBody User user) {
		ResponseEntity<User> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			user.setModifiedOn(date);
			user.setModifiedBy(this.getUserId());
			this.userService.update(user);
			result = new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}
