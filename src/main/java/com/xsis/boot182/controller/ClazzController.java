package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Clazz;
import com.xsis.boot182.service.ClazzService;

@Controller
public class ClazzController extends BaseController{
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ClazzService clazzService;
	
	@RequestMapping("/class.html")
	public String clazz(Model model) {
		List<Clazz> list = new ArrayList<>();
		list = this.clazzService.getList();
		model.addAttribute("classList", list);
		return "class/class";
	}
	
	@RequestMapping("/class/list.html")
	public String list(Model model) {
		List<Clazz> list = new ArrayList<>();
		list = this.clazzService.getList();
		//untuk mengisi 
		model.addAttribute("classList", list);
		return "class/list";
	}
	
	@RequestMapping("/class/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Clazz clazz = this.clazzService.getData(id);
		model.addAttribute("clazz", clazz);
		return "class/delete";
	}
	
	@RequestMapping("/class/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("search");
		List<Clazz> list = new ArrayList<>();
		list = this.clazzService.search(search);
		model.addAttribute("searchList", list);
		return "class/search";
	}
	
	@RequestMapping(value="/api/class/post/", method=RequestMethod.POST)
	public ResponseEntity<Clazz> save(@RequestBody Clazz clazz) {
		ResponseEntity<Clazz> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			clazz.setCreatedOn(date);
			clazz.setCreatedBy(this.getUserId());
			this.clazzService.insert(clazz);
			result = new ResponseEntity<Clazz>(clazz, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Clazz>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/class/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Clazz> delete(@PathVariable(name="id") Long id) {
		try {
			Clazz clazz = this.clazzService.getData(id);
			if (clazz!=null) {
				this.clazzService.delete(clazz);
				return new ResponseEntity<Clazz>(HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);				
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
