package com.xsis.boot182.controller;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.DocumentTest;
import com.xsis.boot182.model.DocumentTestDetail;
import com.xsis.boot182.model.DocumentTestViewModel;
import com.xsis.boot182.model.Question;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.Test;
import com.xsis.boot182.model.TestType;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.DocumentTestService;
import com.xsis.boot182.service.TestService;
import com.xsis.boot182.service.TestTypeService;

@Controller
public class DocumentTestController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private DocumentTestService documentTestService;

	@Autowired
	private TestService testService;

	@Autowired
	private TestTypeService testTypeService;

	@RequestMapping("/document-test.html")
	public String documentTest(Model model) {
		List<DocumentTest> list = new ArrayList<>();
		list = this.documentTestService.getList();
		// tempat menampung data dari model ke view
		model.addAttribute("documentTestList", list);
		return "document-test/index";
	}

	@RequestMapping("/document-test/add.html")
	public String add(Model model) {
		List<Test> listTest = new ArrayList<>();
		listTest = this.testService.getList();
		model.addAttribute("testList", listTest);

		List<TestType> listTestType = new ArrayList<>();
		listTestType = this.testTypeService.getList();
		model.addAttribute("testTypeList", listTestType);

		UUID uuid = UUID.randomUUID();
		String tokens = uuid.toString().replace("-", "").substring(10, 20);
		model.addAttribute("iniToken", tokens);

		return "document-test/add";
	}

	@RequestMapping("/document-test/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		DocumentTest documentTest = this.documentTestService.getData(id);
		model.addAttribute("document", documentTest);

		List<Test> listTest = new ArrayList<>();
		listTest = this.testService.getList();
		model.addAttribute("testList", listTest);

		List<TestType> listTestType = new ArrayList<>();
		listTestType = this.testTypeService.getList();
		model.addAttribute("testTypeList", listTestType);

		UUID uuid = UUID.randomUUID();
		String tokens = uuid.toString().replace("-", "").substring(10, 20);
		model.addAttribute("iniToken", tokens);

		return "document-test/edit";
	}

	@RequestMapping("/document-test/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		DocumentTest documentTest = this.documentTestService.getData(id);
		model.addAttribute("docTest", documentTest);
		return "document-test/delete";
	}

	@RequestMapping(value = "/api/document-test/", method = RequestMethod.POST)
	public ResponseEntity<DocumentTest> save(@RequestBody DocumentTest documentTest) {
		ResponseEntity<DocumentTest> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			documentTest.setCreatedOn(date);
			documentTest.setCreatedBy(this.getUserId());
			documentTest.setIsDelete(0);
			this.documentTestService.insert(documentTest);
			result = new ResponseEntity<DocumentTest>(documentTest, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<DocumentTest>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/document-test/", method = RequestMethod.GET)
	public ResponseEntity<List<DocumentTest>> getAll() {
		ResponseEntity<List<DocumentTest>> result = null;
		try {
			List<DocumentTest> list = this.documentTestService.getList();
			result = new ResponseEntity<List<DocumentTest>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/document-test/", method = RequestMethod.PUT)
	public ResponseEntity<DocumentTestViewModel> update(@RequestBody DocumentTestViewModel documentTestViewModel) {
		ResponseEntity<DocumentTestViewModel> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			documentTestViewModel.setCreatedBy(this.getUserId());
			documentTestViewModel.setCreatedOn(date);
			documentTestViewModel.getDocumentTest().setModifiedOn(date);
			documentTestViewModel.getDocumentTest().setModifiedBy(this.getUserId());
			this.documentTestService.update(documentTestViewModel);
			result = new ResponseEntity<DocumentTestViewModel>(documentTestViewModel, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/document-test/del/", method = RequestMethod.PUT)
	public ResponseEntity<DocumentTest> delete(@RequestBody DocumentTest documentTest) {
		ResponseEntity<DocumentTest> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			documentTest.setDeletedOn(date);
			documentTest.setDeletedBy(this.getUserId());
			documentTest.setIsDelete(1);
			this.documentTestService.update(documentTest);
			result = new ResponseEntity<DocumentTest>(documentTest, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;

	}
}