package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.BatchTest;
import com.xsis.boot182.service.BatchTestService;

@Controller
public class BatchTestController extends BaseController{
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BatchTestService batchTestService;
	
	@RequestMapping(value="/api/batch-test/post/", method=RequestMethod.POST)
	public ResponseEntity<BatchTest> save(@RequestBody BatchTest batchTest) {
		ResponseEntity<BatchTest> result = null;
		
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			batchTest.setCreatedOn(date);
			batchTest.setCreatedBy(this.getUserId());
			this.batchTestService.insert(batchTest);
			result = new ResponseEntity<BatchTest>(batchTest, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<BatchTest>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/batch-test/delete/{batchId}", method=RequestMethod.DELETE)
	public ResponseEntity<BatchTest> delete(@PathVariable(name="batchId") Long batchId) {
		try {
			BatchTest batchTest = this.batchTestService.getBatch(batchId);
			if (batchTest!=null) {
				this.batchTestService.delete(batchTest);
				return new ResponseEntity<BatchTest>(HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);				
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
