package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Role;
import com.xsis.boot182.service.RoleService;

@Controller
public class RoleController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private RoleService roleService;

	@RequestMapping("/role.html")
	public String role(Model model) {
		List<Role> listRole = new ArrayList<>();
		listRole = this.roleService.getList();
		//untuk mengisi 
		model.addAttribute("list", listRole);
		return "role/index";
	}
	
	@RequestMapping("/role/list.html")
	public String list(Model model) {
		List<Role> listRole = new ArrayList<>();
		listRole = this.roleService.getList();
		//untuk mengisi 
		model.addAttribute("list", listRole);
		return "role/list";
	}

	@RequestMapping("/role/add.html")
	public String add() {
		return "role/add";
	}

	@RequestMapping("/role/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Role role = this.roleService.getData(id);
		model.addAttribute("role", role);
		return "role/edit";
	}

	@RequestMapping("/role/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Role role = this.roleService.getData(id);
		model.addAttribute("role", role);
		return "role/delete";
	}
	
	@RequestMapping("/role/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Role> list = new ArrayList<>();
		list = this.roleService.search(search);
		model.addAttribute("searchList", list);
		return "role/search";
	}
	
	//method untuk mengambil name dari form add untuk dibandingkan
	@RequestMapping("/api/role/checkName/{name}")
	public ResponseEntity<String> checkName(@PathVariable(name = "name") String name) {
		ResponseEntity<String> result = null;
		try {
			String cek = Boolean.toString(this.roleService.checkName(name));
			result = new ResponseEntity<String>(cek, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
//		String name = request.getParameter("name");
//		String role = Boolean.toString(this.roleService.checkName(name));	
		return result;
	}

	// request url nya
	@RequestMapping(value = "/api/role/", method = RequestMethod.POST)
	// ResponseEntity = return value nya, <Role> = nama class
	// parameter yg diterima harus berupa RequestBody
	public ResponseEntity<Role> save(@RequestBody Role role, Model model) {
		ResponseEntity<Role> result = null;
		String newCode = roleService.generateCode();
		model.addAttribute("newCode", newCode);
		//this = pengganti current class, 
		if (this.roleService.checkName(role.getName())) {
			log.debug("ERROR CHECKNAME!");
			result = new ResponseEntity<Role>(HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date currentDate = Calendar.getInstance().getTime();
				String date = format.format(currentDate);
				role.setCreatedOn(date);
				role.setCreatedBy(this.getUserId());
				role.setIsDelete(0);
				this.roleService.insert(role);
				result = new ResponseEntity<Role>(role, HttpStatus.CREATED);
			} catch (Exception e) {
				log.debug(e.getMessage(), e);
				result = new ResponseEntity<Role>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return result;
	}

	@RequestMapping(value = "/api/role/update", method = RequestMethod.PUT)
	public ResponseEntity<Role> update(@RequestBody Role role) {
		ResponseEntity<Role> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			role.setModifiedOn(date);
			role.setModifiedBy(this.getUserId());
			this.roleService.update(role);
			result = new ResponseEntity<Role>(role, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/role/del", method = RequestMethod.PUT)
	public ResponseEntity<Role> delete(@RequestBody Role role) {
		ResponseEntity<Role> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			role.setDeletedBy(this.getUserId());
			role.setDeletedOn(date);
			role.setIsDelete(1);
			this.roleService.update(role);
			result = new ResponseEntity<Role>(role, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}