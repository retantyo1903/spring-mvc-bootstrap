package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.TrainerService;

@Controller
public class TrainerController extends BaseController {

private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private TrainerService trainerService;
	
	//Dipanggil dengan Ajax
	@RequestMapping("/trainer.html")
	public String trainer(Model model) {
		List<Trainer> listTrainer = new ArrayList<>();
		listTrainer = this.trainerService.getList();
		model.addAttribute("list", listTrainer);
		model.addAttribute("judul", "TRAINER");
		return "trainer/list";
	}
	
	@RequestMapping("/trainer/isi.html")
	public String isi(Model model) {
		List<Trainer> listTrainer = new ArrayList<>();
		listTrainer = this.trainerService.getList();
		model.addAttribute("list", listTrainer);
		model.addAttribute("judul", "TRAINER");
		return "trainer/isi";
	}
	
	@RequestMapping("/trainer/add.html")
	public String add(Model model) {
		return "trainer/add";
	}
	
	@RequestMapping("/trainer/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Trainer trainer = this.trainerService.getData(id);
		model.addAttribute("trainer", trainer);
		return "trainer/edit";
	}
	
	@RequestMapping("/trainer/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Trainer trainer = this.trainerService.getData(id);
		model.addAttribute("trainer", trainer);
		return "trainer/delete";
	}
	
	@RequestMapping(value="/trainer/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Trainer> list = new ArrayList<>();
		list = this.trainerService.search(search);
		model.addAttribute("searchList", list);
		return "trainer/search";
	}
	
	@RequestMapping(value="/api/trainer/", method=RequestMethod.GET)
	public ResponseEntity<List<Trainer>> getAll() {
		ResponseEntity<List<Trainer>> result = null;
		try {
			List<Trainer> list = this.trainerService.getList();
			result = new ResponseEntity<List<Trainer>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/trainer/", method=RequestMethod.POST)
	public ResponseEntity<Trainer> save(@RequestBody Trainer train) {
		ResponseEntity<Trainer> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			train.setCreatedOn(date);
			train.setCreatedBy(this.getUserId());
			train.setIsDelete(0);
			this.trainerService.insert(train);
			result = new ResponseEntity<Trainer>(train, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Trainer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/trainer/", method=RequestMethod.PUT)
	public ResponseEntity<Trainer> update(@RequestBody Trainer trainer) {
		ResponseEntity<Trainer> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			trainer.setModifiedOn(date);
			trainer.setModifiedBy(this.getUserId());
			this.trainerService.update(trainer);
			result = new ResponseEntity<Trainer>(trainer, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	
	@RequestMapping(value="/api/trainer/delete", method=RequestMethod.PUT)
	public ResponseEntity<Trainer> delete(@RequestBody Trainer trainer) {
		ResponseEntity<Trainer> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			trainer.setDeletedOn(date);
			trainer.setDeletedBy(this.getUserId());
			trainer.setIsDelete(1);
			this.trainerService.update(trainer);
			result = new ResponseEntity<Trainer>(trainer, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/trainer/delete/{id}", method=RequestMethod.GET)
	public ResponseEntity<Trainer> getDelete(@PathVariable(name="id")Long id) {
		ResponseEntity<Trainer> result = null;
		try {
			Trainer trainer = this.trainerService.getData(id);
			result = new ResponseEntity<Trainer>(trainer, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/trainer/{id}", method=RequestMethod.GET)
	public ResponseEntity<Trainer> get(@PathVariable(name="id")Long id) {
		ResponseEntity<Trainer> result = null;
		try {
			Trainer trainer = this.trainerService.getData(id);
			result = new ResponseEntity<Trainer>(trainer, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	
	
}
