package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Category;
import com.xsis.boot182.service.CategoryService;

@Controller
public class CategoryController extends BaseController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	//membuat variabel uhuy dengan tipedata reference
	private CategoryService uhuy;
	
	@RequestMapping("/category.html")
	public String list(Model model) {
		List<Category> list = new ArrayList<>();
		list = this.uhuy.getList();
		//menyimpan list didalam model yang akan dipakai pada view nanti
		model.addAttribute("list", list);
		return "category/category";
	}
	
	@RequestMapping("/category/add.html")
	public String add(Model model) {
		return "category/add";
	}
	
	@RequestMapping("/category/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Category catt = this.uhuy.getData(id);
		model.addAttribute("catt", catt);
		return "category/edit";
	}
	
	@RequestMapping("/category/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Category catt = this.uhuy.getData(id);
		model.addAttribute("catt", catt);
		return "category/delete";
	}
	
	//untuk add
	@RequestMapping(value="/api/category/", method=RequestMethod.POST)
	public ResponseEntity<Category> simpan(@RequestBody Category category){
		ResponseEntity<Category> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			category.setCreatedOn(date);
			category.setCreatedBy(this.getUserId());
			category.setIsDelete(0);
			this.uhuy.insert(category);
			result = new ResponseEntity<Category>(category,HttpStatus.CREATED);
		}catch(Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Category>(HttpStatus.INTERNAL_SERVER_ERROR);			
		}
		return result;
	}
	
	@RequestMapping(value="/api/category/", method=RequestMethod.PUT)
	public ResponseEntity<Category> edit(@RequestBody Category category){
		ResponseEntity<Category> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			category.setModifiedBy(this.getUserId());
			category.setModifiedOn(date);
			this.uhuy.update(category);
			result = new ResponseEntity<Category>(category, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Category>(category, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/category/d", method=RequestMethod.PUT)
	public ResponseEntity<Category> delete(@RequestBody Category category){
		ResponseEntity<Category> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			category.setDeletedBy(this.getUserId());
			category.setDeletedOn(date);
			category.setIsDelete(1);
			this.uhuy.update(category);
			result = new ResponseEntity<Category>(category, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Category>(category, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}