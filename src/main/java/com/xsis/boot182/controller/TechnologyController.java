package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyTrainer;
import com.xsis.boot182.model.TechnologyViewModel;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.TechnologyService;
import com.xsis.boot182.service.TechnologyTrainerService;
import com.xsis.boot182.service.TrainerService;

@Controller
public class TechnologyController extends BaseController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private TechnologyService technologyService;
	
	@Autowired
	private TechnologyTrainerService technologyTrainerService;
	
	@Autowired
	private TrainerService trainerService;

	@RequestMapping("/technology.html")
	public String technology(Model model) {
		List<Technology> listTechnology = new ArrayList<>();
		listTechnology = this.technologyService.getList();
		model.addAttribute("list", listTechnology);
		model.addAttribute("judul", "TECHNOLOGY");
		return "technology/list";
	}
	
	@RequestMapping("/technology/isi.html")
	public String isi(Model model) {
		List<Technology> listTechnology = new ArrayList<>();
		listTechnology = this.technologyService.getList();
		model.addAttribute("list", listTechnology);
		model.addAttribute("judul", "TECHNOLOGY");
		return "technology/isi";
	}

	@RequestMapping("/technology/add.html")
	public String add(Model model) {
		return "technology/add";
	}
	
	
	@RequestMapping("/technology/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Technology technology = this.technologyService.getData(id);
		//untuk menampung variable dan untuk di panggil di view
		model.addAttribute("technology", technology);
		return "technology/edit";
	}
	
	@RequestMapping("/technology/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Technology technology = this.technologyService.getData(id);
		//untuk menampung variable dan untuk di panggil di view
		model.addAttribute("technology", technology);
		return "technology/delete";
	}
	
	@RequestMapping(value="/technology/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Technology> list = new ArrayList<>();
		list = this.technologyService.search(search);
		model.addAttribute("searchList", list);
		return "technology/search";
	}

	@RequestMapping(value = "/api/technology/", method = RequestMethod.GET)
	public ResponseEntity<List<Technology>> getAll() {
		ResponseEntity<List<Technology>> result = null;
		try {
			List<Technology> list = this.technologyService.getList();
			result = new ResponseEntity<List<Technology>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//requestMapping => url bermethod post
	@RequestMapping(value = "/api/technology/", method = RequestMethod.POST)
	// save => nama method bebas
	//Response entyty => return value, Request Body mengambil dari parameter request mapping
	public ResponseEntity<TechnologyViewModel> save(@RequestBody TechnologyViewModel technology) {
		ResponseEntity<TechnologyViewModel> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			technology.setCreatedBy(this.getUserId());
			technology.setCreatedOn(date);
			technology.getTechnology().setCreatedOn(date);
			technology.getTechnology().setCreatedBy(this.getUserId());
			technology.getTechnology().setIsDelete(0);
			this.technologyService.insert(technology);
			result = new ResponseEntity<TechnologyViewModel>(technology, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<TechnologyViewModel>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return result;
	}
	
	@RequestMapping(value="/api/technology/{id}", method=RequestMethod.GET)
	public ResponseEntity<Technology> get(@PathVariable(name="id") Long id) {
		ResponseEntity<Technology> result = null;
		try {
			Technology tech = this.technologyService.getData(id);
			result = new ResponseEntity<Technology>(tech, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/technology/", method=RequestMethod.PUT)
	public ResponseEntity<TechnologyViewModel> update(@RequestBody TechnologyViewModel technology){
		ResponseEntity<TechnologyViewModel> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			technology.setCreatedBy(this.getUserId());
			technology.setCreatedOn(date);
			technology.getTechnology().setModifiedBy(this.getUserId());
			technology.getTechnology().setModifiedOn(date);
			this.technologyService.update(technology);
			result = new ResponseEntity<TechnologyViewModel>(technology, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return result;
	}
	
	//delete
	
	@RequestMapping(value="/api/technology/delete/", method=RequestMethod.PUT)
	public ResponseEntity<Technology> delete(@RequestBody Technology technology){
		ResponseEntity<Technology> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			technology.setDeletedBy(this.getUserId());
			technology.setDeletedOn(date);
			technology.setIsDelete(1);
			this.technologyService.delete(technology);
			result = new ResponseEntity<Technology>(technology, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return result;
	}
	
	/*@RequestMapping(value="/api/technology/{idnyaorang}", method=RequestMethod.DELETE)
	public ResponseEntity<Technology> hapus(@PathVariable(name="idnyaorang") Long id) {
		try {
			Technology technology = this.technologyService.getData(id);
			if (technology!=null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date currentDate = Calendar.getInstance().getTime();
				String date = format.format(currentDate);
				technology.setDeletedBy(this.getUserId());
				technology.setDeletedOn(date);
				technology.setIsDelete(1);
				this.technologyService.delete(technology);
				return new ResponseEntity<Technology>(HttpStatus.OK);
			} else {
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);				
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/
	
}
