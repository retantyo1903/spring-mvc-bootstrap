package com.xsis.boot182.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Room;
import com.xsis.boot182.service.RoomService;

@Controller
public class RoomController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private RoomService roomS;
	
	@RequestMapping(value="/api/room/{idnyaoffice}", method=RequestMethod.GET)
	public ResponseEntity<List<Room>> get(@PathVariable(name="idnyaoffice")Long id){
		ResponseEntity<List<Room>> result = null;
		try {
			List<Room> room = this.roomS.listRoomByOffice(id);
			result = new ResponseEntity<List<Room>>(room, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<List<Room>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}