package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Testimony;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.TestimonyService;

@Controller
public class TestimonyController extends BaseController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private TestimonyService testimonyService;
	
	@RequestMapping("/testimony.html")
	public String testimony(Model model) {
		List<Testimony> listTestimony = new ArrayList<>();
		listTestimony = this.testimonyService.getList();
		model.addAttribute("list", listTestimony);
		model.addAttribute("judul", "TESTIMONY");
		return "testimony/list";
	}
	
	@RequestMapping("/testimony/isi.html")
	public String isi(Model model) {
		List<Testimony> listTestimony = new ArrayList<>();
		listTestimony = this.testimonyService.getList();
		model.addAttribute("list", listTestimony);
		model.addAttribute("judul", "TESTIMONY");
		return "testimony/isi";
	}
	
	@RequestMapping("/testimony/add.html")
	public String add(Model model) {
		return "testimony/add";
	}
	
	@RequestMapping("/testimony/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Testimony testimony = this.testimonyService.getData(id);
		model.addAttribute("testimony", testimony);
		return "testimony/edit";
	}
	
	@RequestMapping("/testimony/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Testimony testimony = this.testimonyService.getData(id);
		model.addAttribute("testimony", testimony);
		return "testimony/delete";
	}
	
	//search
	@RequestMapping(value="/testimony/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Testimony> list = new ArrayList<>();
		list = this.testimonyService.search(search);
		model.addAttribute("searchList", list);
		return "testimony/search";
	}

	//input
	@RequestMapping(value="/api/testimony/", method=RequestMethod.POST)
	public ResponseEntity<Testimony> save(@RequestBody Testimony testimony) {
		ResponseEntity<Testimony> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testimony.setCreatedOn(date);
			testimony.setCreatedBy(this.getUserId());
			testimony.setIsDelete(0);
			this.testimonyService.insert(testimony);
			result = new ResponseEntity<Testimony>(testimony, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Testimony>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//Edit
	@RequestMapping(value="/api/testimony/", method=RequestMethod.PUT)
	public ResponseEntity<Testimony> update(@RequestBody Testimony testimony) {
		ResponseEntity<Testimony> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testimony.setModifiedOn(date);
			testimony.setModifiedBy(this.getUserId());
			this.testimonyService.update(testimony);
			result = new ResponseEntity<Testimony>(testimony, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/testimony/{id}", method=RequestMethod.GET)
	public ResponseEntity<Testimony> get(@PathVariable(name="id")Long id) {
		ResponseEntity<Testimony> result = null;
		try {
			Testimony testimony = this.testimonyService.getData(id);
			result = new ResponseEntity<Testimony>(testimony, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//Delete
	@RequestMapping(value="/api/testimony/delete", method=RequestMethod.PUT)
	public ResponseEntity<Testimony> delete(@RequestBody Testimony testimony) {
		ResponseEntity<Testimony> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testimony.setDeletedOn(date);
			testimony.setDeletedBy(this.getUserId());
			testimony.setIsDelete(1);
			this.testimonyService.update(testimony);
			result = new ResponseEntity<Testimony>(testimony, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/testimony/delete/{id}", method=RequestMethod.GET)
	public ResponseEntity<Testimony> getDelete(@PathVariable(name="id")Long id) {
		ResponseEntity<Testimony> result = null;
		try {
			Testimony testimony = this.testimonyService.getData(id);
			result = new ResponseEntity<Testimony>(testimony, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
