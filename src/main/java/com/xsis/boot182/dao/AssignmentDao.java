package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Assignment;

public interface AssignmentDao {
		public Assignment get(Long id);
		public void insert (Assignment assignment);
		public void update (Assignment assignment);
		public void delete (Assignment assignment);
		public List<Assignment> search(String search);
		public List <Assignment> getList();
}
