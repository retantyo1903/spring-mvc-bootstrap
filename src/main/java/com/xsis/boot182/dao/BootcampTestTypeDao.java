package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.BootcampTestType;

public interface BootcampTestTypeDao {
	public BootcampTestType get(Long id);
	public void insert(BootcampTestType btt);
	public void delete(BootcampTestType btt);
	public void update(BootcampTestType btt);
	public List<BootcampTestType> getList();
	
}
