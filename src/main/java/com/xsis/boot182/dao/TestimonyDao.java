package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Testimony;

public interface TestimonyDao {

	public Testimony get(Long id);

	public void insert(Testimony testimony);

	public void delete(Testimony testimony);

	public void update(Testimony testimony);

	public List<Testimony> getList();
	
	public List<Testimony> search(String search);
}
