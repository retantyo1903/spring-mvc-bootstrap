package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.DocumentTest;

public interface DocumentTestDao {
	public DocumentTest get(Long id);
	
	public void insert(DocumentTest documentTest);

	public void update(DocumentTest documentTest);

	public void delete(DocumentTest documentTest);

	public List<DocumentTest> getList();
}
