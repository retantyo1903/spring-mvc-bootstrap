package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Question;

public interface QuestionDao {
	public Question get(Long id);

	public void insert(Question question);

	public void update(Question question);

	public void delete(Question question);

	public List<Question> getList();
}
