package com.xsis.boot182.dao;

import java.util.List;


import com.xsis.boot182.model.DocumentTestDetail;

public interface DocumentTestDetailDao {
	public DocumentTestDetail get(Long id);

	public void insert(DocumentTestDetail documentTestDetail);

	public void update(DocumentTestDetail documentTestDetail);

	public void delete(DocumentTestDetail documentTestDetail);

	public List<DocumentTestDetail> getList();
	
	public List<DocumentTestDetail> listDocDetailByDocTest(Long idDocumentTest);

}