package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.MenuAccess;

public interface MenuAccessDao {
	public MenuAccess get(Long id);
	public List<MenuAccess> getList();
	public List<MenuAccess> search(Long search);
	public void insert(MenuAccess menuAccess);
	public void delete(MenuAccess menuAccess);
}
