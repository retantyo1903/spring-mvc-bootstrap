package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Feedback;

public interface FeedbackDao {
	public Feedback get(Long id);

	public void insert(Feedback feedback);

	public void update(Feedback feedback);

	public void delete(Feedback feedback);

	public List<Feedback> getList();
}
