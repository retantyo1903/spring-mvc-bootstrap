package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.BootcampTypeDao;
import com.xsis.boot182.model.BootcampType;

@Repository
public class BootcampTypeDaoImpl implements BootcampTypeDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public BootcampType get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(BootcampType.class, id);
	}

	@Override
	public void insert(BootcampType bootcampType) {
		Session session= sessionFactory.getCurrentSession();
		session.save(bootcampType);
	}

	@Override
	public void delete(BootcampType bootcampType) {
		Session session= sessionFactory.getCurrentSession();
		session.update(bootcampType);
		
	}

	@Override
	public void update(BootcampType bootcampType) {
		Session session= sessionFactory.getCurrentSession();
		session.update(bootcampType);
		
	}

	@Override
	public List<BootcampType> getList() {
		Session session= sessionFactory.getCurrentSession();
		//querry yang akan dikirim ke database melalui hibernate
		String hql="select bt from BootcampType bt join User u on u.id =bt.createdBy ";
		// memindah hql ke dalam sesi yang dibuka hibernate
		Query query= session.createQuery(hql);
		return query.getResultList();
	}
	
	@Override
	public List<BootcampType> search(String search) {
		Session session= sessionFactory.getCurrentSession();
		String hql="select bt from BootcampType bt join User u on u.id =bt.createdBy where bt.name like:searchWord ";
		Query query= session.createQuery(hql);
		query.setParameter("searchWord", "%"+search+"%");
		return query.getResultList();
	}
}
