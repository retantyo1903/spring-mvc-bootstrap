package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TechnologyDao;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.Trainer;

@Repository
public class TechnologyDaoImpl implements TechnologyDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Technology get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Technology.class, id);
	}

	@Override
	public void insert(Technology technology) {
		Session session = sessionFactory.getCurrentSession();
		session.save(technology);

	}

	@Override
	public void delete(Technology technology) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(technology);

	}

	@Override
	public void update(Technology technology) {
		Session session = sessionFactory.getCurrentSession();
		session.update(technology);

	}

	@Override
	public List<Technology> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Technology t join User u on u.id = t.createdBy where t.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Technology> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Technology t where t.isDelete=0 and t.name like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		return query.getResultList();
	}
	
}
