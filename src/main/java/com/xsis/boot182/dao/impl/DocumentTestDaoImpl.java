package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.DocumentTestDao;
import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.model.DocumentTest;

@Repository
public class DocumentTestDaoImpl implements DocumentTestDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public DocumentTest get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(DocumentTest.class, id);

	}

	@Override
	public void insert(DocumentTest documentTest) {
		Session session = sessionFactory.getCurrentSession();
		session.save(documentTest);
	}

	@Override
	public void update(DocumentTest documentTest) {
		Session session = sessionFactory.getCurrentSession();
		session.update(documentTest);
	}

	@Override
	public void delete(DocumentTest documentTest) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(documentTest);
	}

	@Override
	public List<DocumentTest> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM DocumentTest d JOIN Test t on t.id = d.testId JOIN TestType tt on tt.id = d.testTypeId WHERE d.isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
