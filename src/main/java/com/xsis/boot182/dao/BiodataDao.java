package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Biodata;

public interface BiodataDao {
	public Biodata get(Long id);
	public void insert(Biodata biodata);
	public void delete(Biodata biodata);
	public void update(Biodata biodata);
	public List<Biodata> search(String search);
	public List<Biodata> getList();
}
