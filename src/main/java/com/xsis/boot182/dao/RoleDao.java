package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Role;

public interface RoleDao {
	public Role get(Long id);
	public void insert(Role role);
	public void delete(Role role);
	public void update(Role role);
	public List<Role> getList();
	public String generateCode();
	public Boolean checkName(String name);
	public List<Role> search(String search);
}
