package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Room;

public interface RoomDao {
	public Room get(Long id);
	public void insert(Room room);
	public void delete(Room room);
	public void update(Room room);
	public List<Room> getList();
	public List<Room> listRoomByOffice(Long officeId);
	public String generateCode();
}
