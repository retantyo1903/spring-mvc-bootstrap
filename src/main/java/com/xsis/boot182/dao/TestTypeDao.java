package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.TestType;

public interface TestTypeDao {
	public TestType get(Long id);
	public void insert(TestType TestType);
	public void delete(TestType TestType);
	public void update(TestType TestType);
	public List<TestType> getList();
}
