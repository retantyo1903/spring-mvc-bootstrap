package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.BootcampType;

public interface BootcampTypeDao {
	public BootcampType get(Long id);
	public void insert(BootcampType bootcampType);
	public void delete(BootcampType bootcampType);
	public void update(BootcampType bootcampType);
	public List<BootcampType> getList();
	public List<BootcampType> search(String search);
}
