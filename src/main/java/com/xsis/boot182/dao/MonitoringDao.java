package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Monitoring;

public interface MonitoringDao {
	public Monitoring get(Long id);
	public void insert(Monitoring monitoring);
	public void delete(Monitoring monitoring);
	public void update(Monitoring monitoring);
	public List<Monitoring> search(String search);
	public List<Monitoring> getList();
}
