package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Test;

public interface TestDao {
	public Test get(Long id);
	public void insert(Test test);
	public void delete(Test test);
	public void update(Test test);
	public List<Test> search(String search);
	public List<Test> getList();
	public List<Test> filterList();
	
}
