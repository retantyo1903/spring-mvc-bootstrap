package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.BatchDao;
import com.xsis.boot182.model.Batch;

@Service
@Transactional
public class BatchServiceImpl implements BatchService{
	
	@Autowired
	private BatchDao batchDao;
	
	@Override
	public List<Batch> getList() {
		return this.batchDao.getList();
	}

	@Override
	public Batch getData(Long id) {
		return this.batchDao.get(id);
	}

	@Override
	public void insert(Batch batch) {
		this.batchDao.insert(batch);
	}

	@Override
	public void delete(Batch batch) {
		this.batchDao.delete(batch);
	}

	@Override
	public void update(Batch batch) {
		this.batchDao.update(batch);
	}

	@Override
	public List<Batch> search(String search) {
		return this.batchDao.search(search);
	}

}
