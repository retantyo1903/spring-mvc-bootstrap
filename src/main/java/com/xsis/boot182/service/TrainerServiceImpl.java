package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TestDao;
import com.xsis.boot182.dao.TrainerDao;
import com.xsis.boot182.model.Trainer;

@Service
@Transactional
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	private TrainerDao trainerDao;
	
	@Override
	public List<Trainer> getList() {
		return this.trainerDao.getList();
	}

	@Override
	public Trainer getData(Long id) {
		return this.trainerDao.get(id);
	}

	@Override
	public void insert(Trainer trainer) {
		this.trainerDao.insert(trainer);
		
	}

	@Override
	public void delete(Trainer trainer) {
		this.trainerDao.delete(trainer);
		
	}

	@Override
	public void update(Trainer trainer) {
		this.trainerDao.update(trainer);
		
	}

	@Override
	public List<Trainer> search(String search) {
		return this.trainerDao.search(search);
	}

}
