package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.QuestionDao;
import com.xsis.boot182.model.Question;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	private QuestionDao questionDao;

	@Override
	public List<Question> getList() {
		return this.questionDao.getList();
	}

	@Override
	public Question getData(Long id) {
		return this.questionDao.get(id);
	}

	@Override
	public void insert(Question question) {
		this.questionDao.insert(question);
	}

	@Override
	public void update(Question question) {
		this.questionDao.update(question);
	}

	@Override
	public void delete(Question question) {
		this.questionDao.delete(question);
	}
}
