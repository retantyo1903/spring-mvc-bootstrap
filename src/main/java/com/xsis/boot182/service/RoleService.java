package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Role;

public interface RoleService {
	public List<Role> getList();
	public Role getData(Long id);
	public void insert(Role role);
	public void delete(Role role);
	public void update(Role role);
	public String generateCode();
	public Boolean checkName(String name);
	public List<Role> search(String search);
}
