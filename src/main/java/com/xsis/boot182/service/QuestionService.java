package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Question;

public interface QuestionService {
	public List<Question> getList();

	public Question getData(Long id);

	public void insert(Question question);

	public void update(Question question);

	public void delete(Question question);
}
