package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.DocumentTest;
import com.xsis.boot182.model.DocumentTestViewModel;

public interface DocumentTestService {
	public List<DocumentTest> getList();

	public DocumentTest getData(Long id);

	public void insert(DocumentTest documentTest);

	public void delete(DocumentTest documentTest);

	public void update(DocumentTest documentTest);

	public void update(DocumentTestViewModel documentTestViewModel);

}
