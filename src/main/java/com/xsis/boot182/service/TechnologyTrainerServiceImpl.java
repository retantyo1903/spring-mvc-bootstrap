package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TechnologyTrainerDao;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyTrainer;

@Service
@Transactional
public class TechnologyTrainerServiceImpl implements TechnologyTrainerService {

	@Autowired
	private TechnologyTrainerDao technologyTrainerDao;
	
	
	@Override
	public TechnologyTrainer getData(Long id) {
		return this.technologyTrainerDao.get(id);
	}
	
	@Override
	public List<TechnologyTrainer> getList() {
		return this.technologyTrainerDao.getList();
	}

	@Override
	public void insert(TechnologyTrainer technologyTrainer) {
		this.technologyTrainerDao.insert(technologyTrainer);

	}

	@Override
	public void delete(TechnologyTrainer technologyTrainer) {
		this.technologyTrainerDao.delete(technologyTrainer);
	}

	@Override
	public void update(TechnologyTrainer technologyTrainer) {
		this.technologyTrainerDao.update(technologyTrainer);

	}

	@Override
	public List<TechnologyTrainer> idTechnology(Long id) {
		return this.technologyTrainerDao.idTechnology(id);
	}

}
