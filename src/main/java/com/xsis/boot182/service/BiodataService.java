package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Biodata;

public interface BiodataService {
	public List<Biodata> getList();
	public List<Biodata> search(String search);
	public Biodata getData(Long id);
	public void insert(Biodata biodata);
	public void delete(Biodata biodata);
	public void update(Biodata biodata);
}
