package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Assignment;

public interface AssignmentService {
	public List <Assignment> getList();
	public List<Assignment> search(String search);
	public Assignment getData(Long id);
	public void insert(Assignment assignment);
	public void update(Assignment assignment);
	public void delete(Assignment assignment);
}
