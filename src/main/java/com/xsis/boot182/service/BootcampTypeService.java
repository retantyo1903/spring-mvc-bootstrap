package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.BootcampType;

public interface BootcampTypeService {
	public List<BootcampType> getList();
	public List<BootcampType> search(String search);
	public BootcampType getData(Long id);
	public void insert (BootcampType bootcampType);
	public void delete (BootcampType bootcampType);
	public void update (BootcampType bootcampType);
}
