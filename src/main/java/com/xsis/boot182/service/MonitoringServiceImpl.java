package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.MonitoringDao;
import com.xsis.boot182.model.Monitoring;

@Service
@Transactional
public class MonitoringServiceImpl implements MonitoringService {

	@Autowired
	private MonitoringDao monitoringDao;
	
	@Override
	public List<Monitoring> getList() {
		return this.monitoringDao.getList();
	}

	@Override
	public List<Monitoring> search(String search) {
		return this.monitoringDao.search(search);
	}

	@Override
	public Monitoring getData(Long id) {
		return this.monitoringDao.get(id);
	}

	@Override
	public void insert(Monitoring monitoring) {
		this.monitoringDao.insert(monitoring);
	}

	@Override
	public void delete(Monitoring monitoring) {
		this.monitoringDao.delete(monitoring);
		
	}

	@Override
	public void update(Monitoring monitoring) {
		this.monitoringDao.update(monitoring);
		
	}

}
