package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TestTypeDao;
import com.xsis.boot182.model.TestType;

@Service
@Transactional
public class TestTypeServiceImpl implements TestTypeService {
	@Autowired
	private TestTypeDao testTypeDao;
	
	@Override
	public List<TestType> getList() {
		return this.testTypeDao.getList();
	}

	@Override
	public TestType getData(Long id) {
		return this.testTypeDao.get(id);
	}

	@Override
	public void insert(TestType testType) {
		this.testTypeDao.insert(testType);
	}

	@Override
	public void delete(TestType testType) {
		this.testTypeDao.delete(testType);
	}

	@Override
	public void update(TestType testType) {
		this.testTypeDao.update(testType);
	}
	
}
