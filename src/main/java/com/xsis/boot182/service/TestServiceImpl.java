package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TestDao;
import com.xsis.boot182.model.Test;

@Service
@Transactional
public class TestServiceImpl implements TestService{

	@Autowired
	private TestDao testDao;
	
	@Override
	public List<Test> getList() {
		return this.testDao.getList();
	}

	@Override
	public Test getData(Long id) {
		return this.testDao.get(id);
	}

	@Override
	public void insert(Test test) {
		this.testDao.insert(test);
	}

	@Override
	public void delete(Test test) {
		this.testDao.delete(test);
	}

	@Override
	public void update(Test test) {
		this.testDao.update(test);
	}

	@Override
	public List<Test> search(String search) {
		return this.testDao.search(search);
	}

	@Override
	public List<Test> filterList() {
		return this.testDao.filterList();
	}

}
