package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.BootcampTestTypeDao;
import com.xsis.boot182.model.BootcampTestType;
@Service
@Transactional
public class BootcampTestTypeServiceImpl implements BootcampTestTypeService {

	@Autowired
	private BootcampTestTypeDao bttDao;
	
	@Override
	public List<BootcampTestType> getList() {
		return this.bttDao.getList();
	}

	@Override
	public BootcampTestType getData(Long id) {
		return this.bttDao.get(id);
	}

	@Override
	public void insert(BootcampTestType btt) {
		this.bttDao.insert(btt);
	}

	@Override
	public void delete(BootcampTestType btt) {
		this.bttDao.delete(btt);
	}

	@Override
	public void update(BootcampTestType btt) {
		this.bttDao.update(btt);
	}

}
