package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Trainer;

public interface TrainerService {

	public List<Trainer> getList();

	public Trainer getData(Long id);

	public void insert(Trainer trainer);

	public void delete(Trainer trainer);

	public void update(Trainer trainer);
	
	public List<Trainer> search(String search);
}
