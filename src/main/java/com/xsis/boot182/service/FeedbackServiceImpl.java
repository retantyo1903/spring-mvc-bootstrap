package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.FeedbackDao;
import com.xsis.boot182.model.Feedback;

@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService {

	@Autowired
	private FeedbackDao feedbackDao;

	@Override
	public List<Feedback> getList() {
		return this.feedbackDao.getList();
	}

	@Override
	public Feedback getData(Long id) {
		return this.feedbackDao.get(id);
	}

	@Override
	public void insert(Feedback feedback) {
		this.feedbackDao.insert(feedback);
	}

	@Override
	public void update(Feedback feedback) {
		this.feedbackDao.update(feedback);
	}

	@Override
	public void delete(Feedback feedback) {
		this.feedbackDao.delete(feedback);
	}

}
