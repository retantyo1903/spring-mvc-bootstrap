package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.MenuAccess;

public interface MenuAccessService {
	public List<MenuAccess> getList();
	public List<MenuAccess> search(Long search);
	public MenuAccess getData(Long id);
	public void insert (MenuAccess menuAccess);
	public void delete (MenuAccess menuAccess);
}
