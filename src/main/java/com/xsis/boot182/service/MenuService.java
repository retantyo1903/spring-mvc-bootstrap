package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Menu;

public interface MenuService {
	public List<Menu> getList();
	public Menu getData(Long id);
	public void insert(Menu menu);
	public void update(Menu menu);
	public void delete(Menu menu);
	public List<Menu> search(String search);
	public String GeneratedCode();
}
