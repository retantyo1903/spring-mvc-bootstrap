package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "T_DOCUMENT_TEST_DETAIL")
public class DocumentTestDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 11, nullable = false)
	private Long id;

	@Column(name = "QUESTION_ID", length = 11, nullable = false)
	private Long questionId;

	@Column(name = "DOCUMENT_TEST_ID", length = 11, nullable = false)
	private Long documentTestId;

	@Column(name = "CREATED_BY", length = 11, nullable = false)
	private Long createdBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_ON", nullable = false)
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name = "QUESTION_ID", insertable = false, updatable = false)
	private Question question;

	@ManyToOne
	@JoinColumn(name = "DOCUMENT_TEST_ID", insertable = false, updatable = false)
	private DocumentTest documentTest;

	public DocumentTestDetail() {
		// TODO Auto-generated constructor stub
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public DocumentTest getDocumentTest() {
		return documentTest;
	}

	public void setDocumentTest(DocumentTest documentTest) {
		this.documentTest = documentTest;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getDocumentTestId() {
		return documentTestId;
	}

	public void setDocumentTestId(Long documentTestId) {
		this.documentTestId = documentTestId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}
}
