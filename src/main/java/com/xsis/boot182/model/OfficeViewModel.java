package com.xsis.boot182.model;

import java.util.List;

public class OfficeViewModel extends Office{
	private Office officeNya;
	private List<Room> room;
	public Office getOfiiceNya() {
		return officeNya;
	}
	public void setOfficeNya(Office officeNya) {
		this.officeNya = officeNya;
	}
	public List<Room> getRoom() {
		return room;
	}
	public void setRoom(List<Room> room) {
		this.room = room;
	}

}
