package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="T_BATCH_TEST")
public class BatchTest {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", length=11, nullable=false)
	private Long id;
	
	@Column(name="BATCH_ID", length=11, nullable=false)
	private Long batchId;
	
	@Column(name="TEST_ID", length=11, nullable=false)
	private Long testId;
	
	@Column(name="CREATED_BY", length=11, nullable=false)
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="BATCH_ID", insertable=false, updatable=false)
	private Batch batch;
	
	@ManyToOne
	@JoinColumn(name="TEST_ID", insertable=false, updatable=false)
	private Test test;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}
}
